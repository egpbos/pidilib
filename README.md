# PIDIMEHS library

[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.1462287.svg)](http://dx.doi.org/10.5281/zenodo.1462287)

This collection of scripts and data files was used in the PIDIMEHS project at the Netherlands eScience Center. PIDIlib is mainly provided to showcase the possibilities of combining Elasticsearch, Pandas and Matplotlib in Python in the study of history of politics and media. PIDIlib's main contents are:

- Ways of querying the PIDIMEHS KB newspaper Elasticsearch instance
- Using Pandas to analyze the queried aggregations
- Visualization using Matplotlib
- Data used for 'indicators of pillarization' in the PIDIMEHS project
- Jupyter notebooks that were used to produce the results presented in the PIDIMEHS technical paper.

The KB newspaper data can be freely obtained from [Delpher](www.delpher.nl). It is not allowed to redistribute this data, so we cannot give access to our own instance. To use PIDIlib, one will have to set up their own Elasticsearch instance. Alternatively, one may contact the KB or SURFsara to request access to the SURFsara instance (which we used as well).

For further information and results see:

- the [NLeSC project site](https://www.esciencecenter.nl/project/pidimehs);
- the [KB-NIAS fellowship page of Huub Wijfjes](https://www.kb.nl/organisatie/kb-fellowship/huub-wijfjes);
- [our paper for the HistoInformatics 2016 workshop proceedings](http://ceur-ws.org/Vol-1632/paper_8.pdf);
- the [newspaper selection and indicator dataset at DANS](http://dx.doi.org/10.17026/dans-xzj-vhgd).


## Installation

To install pidilib, do:

```sh
git clone https://bitbucket.org/egpbos/pidilib.git
cd pidilib
pip install .
```


Run tests (including coverage) with:

```sh
python setup.py test
```

## Contributing

If you want to contribute to the development of pidilib,
have a look at the [contribution guidelines](CONTRIBUTING.rst).


## License

Copyright (c) 2019, Netherlands eScience Center and University of Groningen

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


## Credits

This package was created with [Cookiecutter](https://github.com/audreyr/cookiecutter>) and the [NLeSC/python-template](https://github.com/NLeSC/python-template>).
