#!/usr/bin/env python
import pandas as pd
import datetime
from matplotlib import pyplot as plt
import pidilib.elastic.queries as piq
import pidilib.statistics as pis
import pidilib.publish.style as pistyle
import numpy as np
import os.path

pd_period_freq_from_es = {
    'year': 'A-DEC',
    'month': 'M'
}

# These functions make dataframes from specfic aggregations. The aggregations
# are defined in pidilib.elastic.analyze.

# arts_per_yr_agg, arts_per_mn_agg:
def df_from_arts_per_t_agg(aggs_dict, opdracht, period_min, period_max,
                           opd_aggs_key='aggs', col_name="doc_count"):
    """Only works with defined aggregations arts_per_yr_agg and
    arts_per_mn_agg. `period_min/max` should be a the minimum and
    maximum of the period range (datetime objects, or strings that can be
    interpreted as datetime).
    """
    aggs = opdracht[opd_aggs_key]
    ix_agg1 = list(aggs.keys())[0]  # 0 -> only first (N.B.: dicts have no order!) aggregation in opdracht used
    es_freq = aggs[ix_agg1]['date_histogram']['interval']
    freq = pd_period_freq_from_es[es_freq]

    # full period range index:
    index = pd.period_range(pd.Period(period_min, freq),
                            pd.Period(period_max, freq),
                            freq=freq, name=es_freq)

    df = pd.DataFrame(0, index=index, columns=[col_name])

    buckets = aggs_dict['aggregations'][ix_agg1]['buckets']
    for bucket in buckets:
        dt = datetime.datetime.utcfromtimestamp(bucket['key'] / 1000.0)
        if (dt.year > 1700):
            period = pd.Period(dt, freq)
            df.loc[period] = bucket['doc_count']
            # df.loc[col_name, dt] = bucket['doc_count']

    df = df.loc[pd.Period(period_min, freq):pd.Period(period_max, freq)]  # leave only the values in the defined period range)
    return df


def df_per_zuil_from_phrases_qsq(phrases, period_min, period_max, es, index,
                                 agg=piq.arts_per_yr_agg,
                                 zuil_filt=piq.zuilen_filters,
                                 query_from_phrases=piq.qsq_query_from_phrases):
    query = piq.query_string_query(query_from_phrases(phrases))
    filt = lambda zuil: piq.and_filter([zuil_filt[zuil],
                                        piq.date_range_filter(period_min, period_max)])

    opdracht_zuil = lambda zuil: {"size": 0, 
                                  "query": piq.filtered_query(query, filt(zuil)),
                                  "aggs": agg}

    doc_count_zuil = []

    for zuil_naam in zuil_filt.keys():
        result_date_hist = es.search(index=index, body=opdracht_zuil(zuil_naam))
        df = df_from_arts_per_t_agg(result_date_hist, opdracht_zuil(zuil_naam),
                                    period_min, period_max, col_name=zuil_naam)
        doc_count_zuil.append(df)
        
    doc_count = pd.concat(doc_count_zuil, axis=1)
    doc_count["totaal"] = doc_count.sum(axis=1)
    
    return doc_count


def add_normalized_counts(df, df_all, columns):
    for col in columns:
        df.loc[:, col + "_norm"] = df[col]/df_all[col]


def add_kldiv(df_query, df_all, columns, total_col="totaal"):
    for col in columns:
        df_query.loc[:, col + "_kldiv"] = pis.KL_term(df_query[col], df_all[col],
                                                      df_query[total_col],
                                                      df_all[total_col])


##
## Plotting
##

def figsize_from_size_str(size_str):
    if size_str == "small":
        return (8, 5)
    if size_str == "medium":
        return (12,7.5)
    if size_str == "large":
        return (16,10)


def plot_per_zuil(df, cols, pal, lstyles, lwidths, ax=None, figsize=(16,10),
                  size_str=None, labels=None, steps=True, gridlines="auto",
                  add_legend=True, legend_args={'loc':'best'}, **kwargs):
    """gridlines: 'auto', 'off', or list of coordinates"""
    if steps:
        kwargs['drawstyle'] = "steps-post"
        df_last_step = df.tail(1)
        df_last_step.index = df_last_step.index + 1
        df_local = pd.concat([df, df_last_step])
    else:
        df_local = df.copy(deep=True)

    if size_str is not None:
        figsize = figsize_from_size_str(size_str)
    if ax is None:
        fig, ax = plt.subplots(figsize=figsize)
    for ix, (col, color, style, lw) in enumerate(zip(cols, pal, lstyles,
                                                 lwidths)):
        if labels is None:
            label = col
        else:
            label = labels[ix]
        if gridlines == "auto":
            kwargs['grid'] = None
        elif gridlines == "off":
            # grid=[] necessary to force no grid; default parameter None
            # automatically generates grid lines in Pandas
            kwargs['grid'] = []
        else:
            kwargs['grid'] = gridlines
        df_local[col].plot(color=color, style=style, lw=lw, ax=ax, label=label,
                           **kwargs)
    if add_legend:
        ax.legend(**legend_args)

    ax.autoscale(axis='x', tight=True)
    return ax


def ppzuil(df, style, ax=None, add_legend=True, legend_args={'loc':'best'},
           **kwargs):
    ax = plot_per_zuil(df, style[0], style[1], style[2], style[3], ax=ax,
                       add_legend=add_legend, legend_args=legend_args,
                       **kwargs)
    return ax


def center_year_bins_labels(ax, **kwargs):
    bins = ax.get_xticks()
    ax.set_xticks(bins + 0.5, **kwargs)
    ax.set_xticklabels(np.int32(bins))
    # ax.set_xlim(bins[0], bins[-1])


def plot_kldiv_search_per_zuil(search_phrases, period_min, period_max,
                               normalization_counts, es, index,
                               figsize=(16, 9), output=False,
                               zuilen_filters=piq.zuilen_kranten_filters_v2,
                               style=pistyle.style_dict2,
                               query_from_phrases=piq.qsq_query_from_phrases,
                               title="KL-divergence from total",
                               xlabel=None,
                               ylabel="KL-divergence",
                               agg=piq.arts_per_yr_agg,
                               cache_df_fn=None, add_legend=True, labels=None,
                               ax=None, legend_args={'loc':'best'},
                               zero_line=True, apply_period=False,
                               output_table_filename=None):
    save_to_cache = False
    if cache_df_fn is None or not os.path.isfile(cache_df_fn):
        df = df_per_zuil_from_phrases_qsq(search_phrases, period_min, period_max,
                                          es, index, zuil_filt=zuilen_filters,
                                          query_from_phrases=query_from_phrases,
                                          agg=agg)
        if cache_df_fn is not None:
            save_to_cache = True
    else:
        df = pd.read_pickle(cache_df_fn)
        
    if apply_period:
        df_period = df[period_min:period_max].copy()
        norm_counts_period = normalization_counts[period_min:period_max]
    else:
        df_period = df.copy()
        norm_counts_period = normalization_counts

    columns = style['raw'][0]
    add_kldiv(df_period, norm_counts_period, columns)

    if ax is None:
        fig, ax = plt.subplots(1, 1, figsize=figsize)
    if labels is None:
        labels = style['raw'][0]
    ppzuil(df_period, style['kldiv'], ax=ax, add_legend=add_legend,
           title=title, labels=labels, legend_args=legend_args)

    if zero_line:
        ax.axhline(0, **pistyle.axline_style)

    # center_year_bins_labels(ax)

    if ylabel is not None:
        ax.set_ylabel(ylabel)
    if xlabel is not None:
        ax.set_xlabel(xlabel)

    if output_table_filename is not None:
        df_period.to_html(output_table_filename)
    
    # do this at the end, only after everything succeeded
    if save_to_cache:
        df.to_pickle(cache_df_fn)

    if output:
        return df, fig, ax

def plot_search_per_zuil_kl(search_phrases, period_min, period_max,
                            normalization_counts, columns, es, index, title,
                            ax=None, output=False, figsize=(10, 10),
                            zuilen_filters=piq.zuilen_filters,
                            style=pistyle.style_dict, cache_df_fn=None,
                            add_legend=True,
                            query_from_phrases=piq.qsq_query_from_phrases,
                            ylabel="",
                            agg=piq.arts_per_yr_agg, labels=None,
                            legend_args={'loc':'best'}, **kwargs):
    """Legacy function."""
    if output:
        return plot_kldiv_search_per_zuil(search_phrases, period_min, period_max, normalization_counts, es, index, figsize=figsize, output=output, zuilen_filters=zuilen_filters, style=style, query_from_phrases=query_from_phrases, title=title, ylabel=ylabel, agg=agg, cache_df_fn=cache_df_fn, add_legend=add_legend, labels=labels, ax=ax, legend_args=legend_args, **kwargs)
    else:
        plot_kldiv_search_per_zuil(search_phrases, period_min, period_max, normalization_counts, es, index, figsize=figsize, output=output, zuilen_filters=zuilen_filters, style=style, query_from_phrases=query_from_phrases, title=title, ylabel=ylabel, agg=agg, cache_df_fn=cache_df_fn, add_legend=add_legend, labels=labels, ax=ax, legend_args=legend_args, **kwargs)


raw_title = {'nl': 'artikel telling', 'en': 'raw article counts'}
norm_title = {'nl': 'gedeeld door tot. aantal artikelen',
              'en': "divided by total nr. articles"}
kldiv_title = {'en': "KL-divergence from total",
               'nl': "KL-divergentie t.o.v. totaal"}


def plot_search_per_zuil(search_phrases, period_min, period_max,
                         normalization_counts, columns, es, index,
                         figsize=(16,5), output=False,
                         zuilen_filters=piq.zuilen_filters,
                         style=pistyle.style_dict, lang="en",
                         query_from_phrases=piq.qsq_query_from_phrases,
                         agg=piq.arts_per_yr_agg, xlabel=None,
                         cache_df_fn=None,
                         kl_zero_line=True, **kwargs):
    """
    N.B.: `columns` has become obsolete! We now use directly the columns from
          the style dict.
    """
    if cache_df_fn is None or not os.path.isfile(cache_df_fn):
        df = df_per_zuil_from_phrases_qsq(search_phrases, period_min, period_max,
                                          es, index, zuil_filt=zuilen_filters,
                                          query_from_phrases=query_from_phrases,
                                          agg=agg)
        if cache_df_fn:
            df.to_pickle(cache_df_fn)
    else:
        df = pd.read_pickle(cache_df_fn)

    df_period = df[period_min:period_max].copy()
    norm_counts_period = normalization_counts[period_min:period_max]

    columns = style['raw'][0]
    add_normalized_counts(df_period, norm_counts_period, columns)
    add_kldiv(df_period, norm_counts_period, columns)

    fig, ax = plt.subplots(1, 3, figsize=figsize)
    ppzuil(df_period, style['raw'], ax=ax[0], title=raw_title[lang], **kwargs)
    ppzuil(df_period, style['norm'], ax=ax[1], title=norm_title[lang], **kwargs)
    ppzuil(df_period, style['kldiv'], ax=ax[2], title=kldiv_title[lang], **kwargs)
    for ix, axis in enumerate(ax):
        if xlabel is not None:
            ax[ix].set_xlabel(xlabel)

    if kl_zero_line:
        ax[2].axhline(0, **pistyle.axline_style)

    if output:
        return df_period, fig, ax


def plot_search_per_zuil_select_orgs(org_df, org_df_selection, period_min,
                                     period_max, normalization_counts, columns,
                                     es, index, output=False,
                                     zuilen_filters=piq.zuilen_filters,
                                     figsize=(16, 5), style=pistyle.style_dict,
                                     cache_df_fn=None):
    selection_df = org_df[org_df_selection]
    selection_phrases = piq.phrases_str_from_organisations(selection_df)
    return plot_search_per_zuil(selection_phrases, period_min, period_max,
                                normalization_counts, columns, es, index,
                                output=output, zuilen_filters=zuilen_filters,
                                figsize=figsize, style=style,
                                cache_df_fn=cache_df_fn)




def plot_search_zuilen_select_orgs(org_df, org_df_selection_dict, title_dict,
                                   period_min, period_max,
                                   normalization_counts, columns, es, index,
                                   layout=None, tile_size=5, output=False,
                                   zuilen_filters=piq.zuilen_filters,
                                   style=pistyle.style_dict,
                                   cache_df_fn_base=None, add_legends=[0,],
                                   labels=None, xlabel=None, ax=None,
                                   legends_args={0:{'loc':'best'}}, **kwargs):
    """
    N.B.: `columns` has become obsolete! We now use directly the columns from
          the style dict.
    """
    if layout is None:
        tiles = int(np.ceil(np.sqrt(len(org_df_selection_dict))))
        layout = (tiles, tiles)

    if tile_size is not None:
        figsize = (tile_size*layout[1], tile_size*layout[0])
    else:
        figsize = None
    if ax is None:
        fig, ax = plt.subplots(layout[0], layout[1], figsize=figsize)
    try:
        ax = ax.flatten()
    except AttributeError:
        ax = [ax]

    for ix, key in enumerate(org_df_selection_dict):
        selection_df = org_df[org_df_selection_dict[key]]
        if len(selection_df) > 0:
            selection_phrases = piq.phrases_str_from_organisations(selection_df)
            if cache_df_fn_base is None:
                cache_df_fn = None
            else:
                fn, ext = os.path.splitext(cache_df_fn_base)
                cache_df_fn = fn + "_%i" % ix + ext
            if ix in add_legends:
                add_legend = True
                legend_args = legends_args[ix]
            else:
                add_legend = False
                legend_args = {'loc':'best'}
            plot_search_per_zuil_kl(selection_phrases, period_min, period_max,
                                    normalization_counts, None, es, index,
                                    title_dict[key], ax=ax[ix], output=output,
                                    zuilen_filters=zuilen_filters, style=style,
                                    cache_df_fn=cache_df_fn,
                                    add_legend=add_legend, labels=labels,
                                    legend_args=legend_args, **kwargs)
        else:
            ax[ix].axis('off')

    for ix, axis in enumerate(ax):
        # if ix >= (layout[0] * (layout[1] - 1)):  # bottom row
        if xlabel is not None:
            ax[ix].set_xlabel(xlabel)
        # else:
            # ax[ix].set_xlabel("")


def plot_search_alle_zuilen_select_org_type(org_df, org_type, period_min, period_max,
                                            normalization_counts, columns, es, index,
                                            layout=(2,2), tile_size=6,
                                            zuilen_filters=piq.zuilen_filters,
                                            style=pistyle.style_dict,
                                            suptitle=None, lang="nl",
                                            cache_df_fn_base=None,
                                            add_legends=[0,], labels=None,
                                            xlabel=None, ax=None,
                                            legends_args={0:{'loc':'best'}},
                                            plot_zuilen=['kat', 'soc', 'pro',
                                                         'lib'],
                                            subtitles=True, **kwargs):
    """
    N.B.: `columns` has become obsolete! We now use directly the columns from
          the style dict.
    """
    if isinstance(org_type, str):
        org_df_type = org_df[(org_df["type"] == org_type)]
        org_type_str = ": " + org_type
    else:  # org_type is list or other iterator (but not string, which is also an iterator!)
        org_df_type = org_df[(org_df["type"].isin(org_type))]
        org_type_str = ""

    if suptitle is None:
        if isinstance(org_type, str):
            suptitle = ""
        else:  # org_type is list or other iterator (but not string, which is also an iterator!)
            if lang == "nl":
                suptitle = "Organisatietypes: " + ", ".join(org_type)
            elif lang == "en":
                suptitle = "Organization types: " + ", ".join(org_type)

    lib = (org_df_type["zuil"] == "lib")
    soc = (org_df_type["zuil"] == "soc")
    kat = (org_df_type["zuil"] == "kat")
    pro = (org_df_type["zuil"] == "pro")
    zuil_orgs = {'kat': kat, 'soc': soc, 'pro': pro, 'lib': lib}
    if subtitles:
        if lang == "nl":
            zuil_titles = {'kat': 'katholieke organisaties' + org_type_str,
                           'soc': 'socialistische organisaties' + org_type_str,
                           'pro': 'protestantse organisaties' + org_type_str,
                           'lib': 'liberale organisaties' + org_type_str}
        elif lang == "en":
            zuil_titles = {'kat': 'Catholic organizations' + org_type_str,
                           'soc': 'Socialist organizations' + org_type_str,
                           'pro': 'Protestant organizations' + org_type_str,
                           'lib': 'Liberal organizations' + org_type_str}
    else:
        zuil_titles = {k: "" for k in zuil_orgs.keys()}

    zuil_orgs = {k: v for k, v in zuil_orgs.items() if k in plot_zuilen}
    zuil_titles = {k: v for k, v in zuil_titles.items() if k in
                   plot_zuilen}

    plot_search_zuilen_select_orgs(org_df_type, zuil_orgs, zuil_titles,
                                   period_min, period_max, normalization_counts,
                                   pistyle.cols, es, index, layout=layout,
                                   tile_size=tile_size,
                                   zuilen_filters=zuilen_filters, style=style,
                                   cache_df_fn_base=cache_df_fn_base,
                                   add_legends=add_legends, labels=labels,
                                   xlabel=xlabel, ax=ax,
                                   legends_args=legends_args, **kwargs)
    plt.suptitle(suptitle)


def plot_search_per_zuil_select_pers(pers_df, pers_df_selection, period_min, period_max, normalization_counts,
                                     columns, es, index, output=False, figsize=(16, 5),
                                     zuilen_filters=piq.zuilen_filters,
                                     style=pistyle.style_dict,
                                     cache_df_fn=None):
    selection_df = pers_df[pers_df_selection]
    selection_phrases = piq.phrases_str_from_persons(selection_df)
    plot_search_per_zuil(selection_phrases, period_min, period_max, normalization_counts,
                         columns, es, index, output=output, figsize=figsize,
                         zuilen_filters=zuilen_filters, style=style,
                         cache_df_fn=cache_df_fn)


def plot_search_per_zuil_select_pers_simple(pers_df, pers_df_selection, period_min, period_max, normalization_counts,
                                     columns, es, index, output=False, figsize=(16, 5),
                                     zuilen_filters=piq.zuilen_filters,
                                     style=pistyle.style_dict,
                                     cache_df_fn=None):
    selection_df = pers_df[pers_df_selection]
    selection_phrases = piq.phrases_str_from_persons_simple(selection_df)
    plot_search_per_zuil(selection_phrases, period_min, period_max, normalization_counts,
                         columns, es, index, output=output, figsize=figsize,
                         zuilen_filters=zuilen_filters, style=style,
                         cache_df_fn=cache_df_fn)


def shorten_string(string, maxlen):
    if len(string) > maxlen:
        return string[:maxlen] + "..."
    else:
        return string


def plot_search_per_line_per_zuil(search_phrases, period_min, period_max,
                                  normalization_counts, columns, es, index,
                                  figwidth=16, rowheight=5, output=False,
                                  ax_cols=3, title_maxlen=30,
                                  zuilen_filters=piq.zuilen_filters,
                                  qs_from_line=piq.querystr_from_igterm_line,
                                  style=pistyle.style_dict,
                                  cache_df_fn_base=None):
    """
    N.B.: `columns` has become obsolete! We now use directly the columns from
          the style dict.
    """
    dfs = {}
    columns = style['raw'][0]
    for ix, line in enumerate(search_phrases.split('\n')):
        if cache_df_fn_base is None:
            cache_df_fn = None
        else:
            fn, ext = os.path.splitext(cache_df_fn_base)
            cache_df_fn = fn + "_%i" % ix + ext

        if cache_df_fn is None or not os.path.isfile(cache_df_fn):
            df = df_per_zuil_from_phrases_qsq(line, period_min, period_max, es,
                                              index, zuil_filt=zuilen_filters,
                                              query_from_phrases=qs_from_line)
            if cache_df_fn:
                df.to_pickle(cache_df_fn)
        else:
            df = pd.read_pickle(cache_df_fn)

        add_kldiv(df, normalization_counts, columns)
        dfs[line] = df

    ax_rows = int(np.ceil(float(len(dfs))/ax_cols))
    figsize = (figwidth, ax_rows*rowheight)
    fig, ax = plt.subplots(ax_rows, ax_cols, figsize=figsize)
    for ix, (line, df) in enumerate(dfs.items()):
        ppzuil(df, style['kldiv'], ax=ax.flat[ix],
               title=shorten_string(line, title_maxlen))
    for jx in range(ix+1, ax_rows*ax_cols):
        ax.flat[jx].axis('off')

    if output:
        return dfs, fig, ax

