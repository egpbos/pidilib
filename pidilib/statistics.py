import numpy as np

def KL_term(T_st, T_s, T_t, T):
    """
    Kullback-Leibler (0 if any of the arguments are 0).
      T      : Tokens total.                        (in tsv: background tokens)
    @ T_t    : Tokens matching term t.         (in tsv: background term tokens)
      T_s    : Tokens in this subset s.                   (in tsv: term tokens)
    @ T_st   : Tokens matching term t in subset s.              (in tsv: abs:t)
    
    T_st and T_t can be either scalars or arrays. They must be the same size.
    T_s and T can be scalars or arrays of the same size as T_st and T_t.

    T = tokens: any match to the search query.
    t = term: a specific match as represented in columns, e.g. (A\.?R\.?P|C\.?H\.?U)
    s = subset/slice: matching a set of papers, e.g. "socialistisch"e papers.

    (Used @ to mean "repeated for each term".)
    """
    KL_t = (T_st/np.float64(T_s)) * np.log2( (T_st/np.float64(T_s)) / (T_t/np.float64(T)) )
    # filter out zero terms:
    KL_t = np.where((T_st == 0) | (T_t == 0), np.nan * np.ones_like(KL_t), KL_t)
    return KL_t