#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import dateutil
import datetime
import os.path


def convert_raw_lijsttrekkers(raw_data):
    data = []
    for line in raw_data.splitlines():
        try:
            jaar = int(line)
        except ValueError:
            if line.strip():  # skip empty lines ("" is equivalent to False)
                data.extend(splits_lijsttrekkers(line, jaar))
    return data


def splits_lijsttrekkers(namen_str, jaar):
    partijnaam, partijpersonen_str = namen_str.split(":")
    partijpersonen = [pers.strip() for pers in partijpersonen_str.strip().split(',')]
    data = []
    for pers in partijpersonen:
        namen = pers.split()
        voorletters = namen[0]
        if u"(" in pers:
            roepnaam = namen[1].strip("()")
            achternaam = " ".join(namen[2:])
        else:
            roepnaam = ""
            achternaam = " ".join(namen[1:])
        data.append({'voorletters': voorletters,
                     'roepnaam': roepnaam,
                     'complete achternaam': achternaam,
                     'partij': partijnaam,
                     'jaar': jaar})
    return data


lijsttrekkers_1918_by_hand = [
    {'voorletters': 'J.G.', 'roepnaam': '', 'complete achternaam': 'Scheurer', 'partij(en)/fractie(s)': 'ARP', 'jaar': 1918},
    {'voorletters': 'Th.', 'roepnaam': 'Theo', 'complete achternaam': 'Heemskerk', 'partij(en)/fractie(s)': 'ARP', 'jaar': 1918},
    {'voorletters': 'A.W.F.', 'roepnaam': '', 'complete achternaam': 'Idenburg', 'partij(en)/fractie(s)': 'ARP', 'jaar': 1918},
    {'voorletters': 'A.F.', 'roepnaam': '', 'complete achternaam': 'De Savornin Lohman', 'partij(en)/fractie(s)': 'CHU', 'jaar': 1918},
    {'voorletters': 'J.A.', 'roepnaam': '', 'complete achternaam': 'Loeff', 'partij(en)/fractie(s)': 'RK', 'jaar': 1918},
    {'voorletters': 'W.H.', 'roepnaam': '', 'complete achternaam': 'Bogaardt', 'partij(en)/fractie(s)': 'RK', 'jaar': 1918},
    {'voorletters': 'A.I.M.J.', 'roepnaam': '', 'complete achternaam': 'Van Wijnbergen', 'partij(en)/fractie(s)': 'RK', 'jaar': 1918},
    {'voorletters': 'M.J.C.M.', 'roepnaam': '', 'complete achternaam': 'Kolkman', 'partij(en)/fractie(s)': 'RK', 'jaar': 1918},
    {'voorletters': 'J.B.', 'roepnaam': '', 'complete achternaam': 'van Dijk', 'partij(en)/fractie(s)': 'RK', 'jaar': 1918},
    {'voorletters': 'J.B.', 'roepnaam': '', 'complete achternaam': 'Bomans', 'partij(en)/fractie(s)': 'RK', 'jaar': 1918},
    {'voorletters': 'P.F.', 'roepnaam': '', 'complete achternaam': 'Fruytier', 'partij(en)/fractie(s)': 'RK', 'jaar': 1918},
    {'voorletters': 'A.H.J.', 'roepnaam': '', 'complete achternaam': 'Engels', 'partij(en)/fractie(s)': 'RK', 'jaar': 1918},
    {'voorletters': 'W.H.', 'roepnaam': '', 'complete achternaam': 'Nolens', 'partij(en)/fractie(s)': 'RK', 'jaar': 1918},
    {'voorletters': 'P.', 'roepnaam': '', 'complete achternaam': 'Rink', 'partij(en)/fractie(s)': 'Lib.Unie', 'jaar': 1918},
    {'voorletters': 'P.', 'roepnaam': '', 'complete achternaam': 'Otto', 'partij(en)/fractie(s)': 'Lib.Unie', 'jaar': 1918},
    {'voorletters': 'C.', 'roepnaam': '', 'complete achternaam': 'Lely', 'partij(en)/fractie(s)': 'Lib.Unie', 'jaar': 1918},
    {'voorletters': 'D.', 'roepnaam': '', 'complete achternaam': 'Fock', 'partij(en)/fractie(s)': 'Lib.Unie', 'jaar': 1918},
    {'voorletters': 'R.R.L.', 'roepnaam': '', 'complete achternaam': 'De Muralt', 'partij(en)/fractie(s)': 'Lib.Unie', 'jaar': 1918},
    {'voorletters': 'H.C.', 'roepnaam': '', 'complete achternaam': 'Dresselhuys', 'partij(en)/fractie(s)': 'Vrije Lib.', 'jaar': 1918},
    {'voorletters': 'M.W.F.', 'roepnaam': '', 'complete achternaam': 'Treub', 'partij(en)/fractie(s)': 'Econ. Bond', 'jaar': 1918},
    {'voorletters': 'H.P.', 'roepnaam': '', 'complete achternaam': 'Marchant', 'partij(en)/fractie(s)': 'VDB', 'jaar': 1918},
    {'voorletters': 'Th.M.', 'roepnaam': 'Theo', 'complete achternaam': 'Ketelaar', 'partij(en)/fractie(s)': 'VDB', 'jaar': 1918},
    {'voorletters': 'J.', 'roepnaam': '', 'complete achternaam': 'Oudegeeest', 'partij(en)/fractie(s)': 'SDAP', 'jaar': 1918},
    {'voorletters': 'A.B.', 'roepnaam': '', 'complete achternaam': 'Kleerekoper', 'partij(en)/fractie(s)': 'SDAP', 'jaar': 1918},
    {'voorletters': 'J.', 'roepnaam': '', 'complete achternaam': 'ter Laan', 'partij(en)/fractie(s)': 'SDAP', 'jaar': 1918},
    {'voorletters': 'K.', 'roepnaam': '', 'complete achternaam': 'ter Laan', 'partij(en)/fractie(s)': 'SDAP', 'jaar': 1918},
    {'voorletters': 'P.J.', 'roepnaam': '', 'complete achternaam': 'Troelstra', 'partij(en)/fractie(s)': 'SDAP', 'jaar': 1918},
    {'voorletters': 'J.E.W.', 'roepnaam': '', 'complete achternaam': 'Duijs', 'partij(en)/fractie(s)': 'SDAP', 'jaar': 1918},
    {'voorletters': 'F.W.N.', 'roepnaam': '', 'complete achternaam': 'Hugenholtz', 'partij(en)/fractie(s)': 'SDAP', 'jaar': 1918},
    {'voorletters': 'W.P.G.', 'roepnaam': '', 'complete achternaam': 'Helsdingen', 'partij(en)/fractie(s)': 'SDAP', 'jaar': 1918},
    {'voorletters': 'J.W.', 'roepnaam': '', 'complete achternaam': 'Albarda', 'partij(en)/fractie(s)': 'SDAP', 'jaar': 1918},
    {'voorletters': 'J.H.A.', 'roepnaam': '', 'complete achternaam': 'Schaper', 'partij(en)/fractie(s)': 'SDAP', 'jaar': 1918},
    {'voorletters': 'W.C.', 'roepnaam': '', 'complete achternaam': 'De Jonge', 'partij(en)/fractie(s)': 'SDAP', 'jaar': 1918}
]


raw_lijsttrekkers = u"""
1918
ARP: J.G. Scheurer, Th. (Theo) Heemskerk, A.W.F. Idenburg

CHU: A.F. De Savornin Lohman

RK: J.A. Loeff, W.H. Bogaardt, A.I.M.J. Van Wijnbergen, M.J.C.M. Kolkman, J.B. van Dijk, J.B. Bomans, P.F. Fruytier, A.H.J. Engels, W.H. Nolens

Lib.Unie: P. Rink, P. Otto, C. Lely, D. Fock, R.R.L. De Muralt

Vrije Lib.: H.C. Dresselhuys

Econ. Bond: M.W.F. Treub

VDB: H.P. Marchant, Th.M. (Theo) Ketelaar

SDAP: J. Oudegeeest, A.B. Kleerekoper, J. ter Laan, K. ter Laan, P.J. Troelstra, J.E.W. Duijs, F.W.N. Hugenholtz, W.P.G. Helsdingen, J.W. Albarda, J.H.A. Schaper, W.C. De Jonge

1922
ARP: H. Colijn, J. Schouten, V.H. Rutgers

CHU: J. Schokking

RK: A.F.O. Van Sasse van Ysselt, A.B.G.M. Van Rijckevorsel, A.I.M.J. Van Wijnbergen, H. Stulemeyer, D.A.P.N. Koolen, M.J.C.M. Kolkman, C.J. Kuiper, J.B. Van Dijk, J.B. Bomans, P.F. Fruytier, A.H.J. Engels, A.C.A. Van Vuuren, W.H. Nolens

VDB: H.P. Marchant

VB: A.C. Visser van IJzendoorn, H.C. Dresselhuys, S.E.B. Bierema, A. Van Gijn, R.R.L. De Muralt

VB+: Abr. Staalman, H. ter Hall

SDAP: W.H. Vliegen, A.B. Kleerekoper, J. ter Laan, K. ter Laan, P.J. Troelstra, J.E.W. Duijs, F.W.N. Hugenholtz, G.W. Sannes, J.W. Albarda, J.H.A. Schaper

1925
ARP: H. Colijn

CHU: J. Schokking

RKSP: A.F.O. Van Sasse van Ysselt, A.B.G.M. Van Rijckevorsel, A.I.M.J. Van Wijnbergen, D.A.P.N. Koolen, J.B. Van Dijk, J.B. Bomans, P.F. Fruytier, A.C.A. Van Vuuren, A.H.J. Engels, W.H. Nolens

VDB: H.P. Marchant

VB: A.G.Æ. Van Rappard, H.C. Dresselhuys, H.J. Knottenbelt, W. Boissevain, G.A. Boon, A. Van Gijn, I.H.J. Vos, P. Droogleever Fortuyn

VB+: Abr. Staalman, H.J. ter Hall

SDAP: W.H. Vliegen, A.B. Kleerekoper, J. ter Laan, K. ter Laan, A.H. Gerhard, J.E.W. Duijs, T. (Theo) Van der Waerden, G.W. Sannes, J.W. Albarda, J.H.A. Schaper

1929
ARP: H. Colijn, Th. (Theo) Heemskerk, J. Schouten, J.J.C. van Dijk, J. Severijn, H. Visscher, C. Smeenk, C. van den Heuvel, L.F. Duymaer van Twist, J.A. De Wilde, E.J. Beumer, A. Zijlstra

CHU: J. Schokking

RKSP: A.N. Fleskens, L.J.M. Feber, J.R.H. Van Schaik, Ch.J.M. Ruijs de Beerenbrouck, A.C.A. Van Vuuren, Ch.L. Van de Bilt, J.G. Suring, P.J.M. Aalberse, W.H. Nolens

VDB: H.P. Marchant

VB: S.E.B. Bierema, Van de Bilt, G.A. Boon, I.H.J. Vos, A.G.Æ. Van Rappard, H.J. Knottenbelt

SDAP: W.H. Vliegen, A.B. Kleerekoper, J. ter Laan, K. ter Laan, A.H. Gerhard, J.E.W. Duijs, T. (Theo) Van der Waerden, G.W. Sannes, J.W. Albarda, J.H.A. Schaper

1933
ARP: H. Colijn

CHU: D.J. De Geer

RKSP: Ch.L. Van de Bilt, J.R.H. Van Schaik, A.J. Loerakker, L.N. Deckers, H.G.M. Hermans, C.M.J.F. Goseling, C.J. Kuiper, P.J.M. Aalberse, J.G. Suring

VDB: H.P. Marchant

VB: S.E.B. Bierema, G.A. Boon, I.H.J. Vos, A.G.Æ. Van Rappard, O.C.A. Van Lidth de Jeude, H.D. Louwes, C.J. Van Kempen, H.J. Knottenbelt, B.D. Eerdmans, W.C. Wendelaar

SDAP: W.H. Vliegen, H.J. Van Braambeek, J. ter Laan, W. Drees, K. ter Laan, J.W. Albarda, J.E.W. Duijs, T. (Theo) Van der Waerden, P.F. Hiemstra, W. Van der Sluis, A. Van der Heide, J.H.A. Schaper

1937
ARP: H. Colijn

CHU: D.J. De Geer

RKSP: L.N. Deckers, C.M.J.F. Goseling, H.W.E. Moller, P.J.M. Aalberse, L.J.C. Beaufort, H.G.M. Hermans

VDB: P.J. Oud

VB: S.E.B. Bierema, O.C.A. Van Lidth de Jeude, I.H.J. Vos, W.C. Wendelaar

SDAP: W. Drop, W. Van der Sluis, J. ter Laan, W. Drees, J.W. Albarda
"""

lijsttrekkers_json = convert_raw_lijsttrekkers(raw_lijsttrekkers)
lijsttrekkers_df = pd.DataFrame(lijsttrekkers_json)

# To check if they're indeed the same:
# ene = sorted([ding for ding in lijsttrekkers_json if ding['jaar'] == 1918])
# andere = sorted(lijsttrekkers_1918_by_hand)
# ene == andere


# bewindspersonen
# N.B.: PDC text file encoding is windows-1252!
try:
    with open(os.path.abspath(os.path.dirname(__file__)) + "/pdc/bewindslieden_1900-2014_20140930.csv", 'br') as fh:
        bewindspersonen_df = pd.read_csv(fh, sep=";", parse_dates=[11,12],
                                         encoding='cp1252')

    prep = bewindspersonen_df['prepositie'].fillna('')
    compl_achter = prep + " " + bewindspersonen_df['achternaam']
    # and strip the complete family names to remove spaces with empty prepositions:
    # N.B.: In python 2 you cannot use str.strip for unicode strings, must use unicode.strip! But this is Python 3
    bewindspersonen_df['complete achternaam'] = compl_achter.apply(str.strip)
    # rename party column:
    bewindspersonen_df.rename(columns={'partij(en)/fractie(s)': 'partij'}, inplace=True)
    # fill roepnamen with empty strings:
    bewindspersonen_df.roepnaam.fillna('', inplace=True)
except FileNotFoundError:
    print("PDC bewindspersonen csv file not found, please place the file at pidilib/data/pdc/bewindslieden_1900-2014_20140930.csv")


# kamerleden
# N.B.: PDC text file encoding is windows-1252!
try:
    with open(os.path.abspath(os.path.dirname(__file__)) + "/pdc/tweedekamerleden_1900-2014_20140930.csv", 'br') as fh:
        tweedekamerleden_df = pd.read_csv(fh, sep=";", parse_dates=[8,9],
                                          encoding='cp1252')

    prep = tweedekamerleden_df['prepositie'].fillna('')
    compl_achter = prep + " " + tweedekamerleden_df['achternaam']
    # and strip the complete family names to remove spaces with empty prepositions:
    # N.B.: In python 2 you cannot use str.strip for unicode strings, must use unicode.strip! But this is Python 3
    tweedekamerleden_df['complete achternaam'] = compl_achter.apply(str.strip)
    # rename party column:
    tweedekamerleden_df.rename(columns={'partij(en)/fractie(s)': 'partij'}, inplace=True)
    # fill roepnamen with empty strings:
    tweedekamerleden_df.roepnaam.fillna('', inplace=True)
except FileNotFoundError:
    print("PDC tweedekamerleden csv file not found, please place the file at pidilib/data/pdc/tweedekamerleden_1900-2014_20140930.csv")


# lijsttrekkers post-war
lijsttrekkers_postwar = {
    'ARP': [
        ["Dr. J. Schouten", "1946", "2-7-1956"],
        ["Dr. J. Zijlstra", "14-7-1956", "2-10-1956"],
        ["Dr. J. A. H. J. S. Bruins Slot", "3-10-1956", "6-6-1963"],
        ["H. van Eijsden", "6-6-1963", "16-7-1964"],
        ["J. Smallenbroek", "17-7-1964", "13-4-1965"],
        ["B. Roolvink", "14-4-1965", "22-2-1967"],
        ["Mr. B. W. Biesheuvel", "23-2-1967", "5-7-1971"],
        ["Mr. W. Aantjes", "6-7-1971", "6-12-1972"],
        ["Mr. B. W. Biesheuvel", "7-12-1972", "6-3-1973"],
        ["Mr. W. Aantjes", "7-3-1973", "7-6-1977"]
    ],

    'CHU': [
        ["Dr. H. W. Tilanus", "17-5-1946", "5-6-1963"],
        ["Mr. H. K. J. Beernink", "6-6-1963", "4-4-1967"],
        ["J. T. Mellema", "5-4-1967", "31-7-1968"],
        ["Drs. A. D. W. Tilanus", "1-8-1968", "26-6-1969"],
        ["J. T. Mellema", "27-6-1969", "10-5-1971"],
        ["Drs. B. J. Udink", "11-5-1971", "5-7-1971"],
        ["Dr. R. J. H. Kruisinga", "6-7-1971", "26-7-1971"],
        ["J. T. Mellema", "27-7-1971", "31-3-1972"],
        ["Drs. A. D. W. Tilanus", "1-4-1972", "30-6-1973"],
        ["Dr. R. J. H. Kruisinga", "1-7-1973", "7-6-1977"]
    ],

    'CDA': [
        ["Mr. A. A. M. van Agt", "8-8-1977", "18-12-1977"],
        ["Mr. W. Aantjes", "20-12-1977", "6-11-1978"],
        ["Drs. R. F. M. Lubbers", "13-12-1978", "9-6-1981"],
        ["Mr. A. A. M. van Agt", "10-6-1981", "26-8-1981"],
        ["Drs. R. F. M. Lubbers", "27-8-1981", "3-11-1982"],
        ["Dr. B. de Vries", "4-11-1982", "21-5-1986"],
        ["Drs. R. F. M. Lubbers", "22-5-1986", "14-7-1986"],
        ["Dr. B. de Vries", "15-7-1986", "13-9-1989"],
        ["Drs. R. F. M. Lubbers", "14-9-1989", "6-11-1989"]
    ],

    'KVP': [
        ["Mr. C. P. M. Romme", "4-6-1946", "13-6-1961"],
        ["W. J. Andriessen", "13-6-1961", "2-10-1961"],
        ["Dr. W. L. P. M. de Kort", "2-10-1961", "10-12-1963"],
        ["Drs. W. K. N. Schmelzer", "10-12-1963", "10-5-1971"],
        ["Dr. G. H. Veringa", "11-5-1971", "31-12-1971"],
        ["Mr. F. H. J. J. Andriessen", "1-1-1972", "7-6-1977"]
    ],

    'PvdA': [
        ["Mr. M. van der Goes van Naters", "1946", "1-1951"],
        ["Mr. L. A. Donker", "1-1951", "9-1952"],
        ["Mr. J. A. W. Burger", "9-1952", "25-9-1962"],
        ["Dr. ir. A. Vondeling", "25-9-1962", "13-4-1965"],
        ["Drs. G. M. Nederhorst", "14-4-1965", "22-2-1967"],
        ["Drs. J. M. den Uyl", "23-2-1967", "10-5-1973"],
        ["Drs. E. van Thijn", "15-5-1973", "15-1-1978"],
        ["Drs. J. M. den Uyl", "16-1-1978", "10-9-1981"],
        ["W. Meijer", "11-9-1981", "16-9-1982"],
        ["Drs. J. M. den Uyl", "16-9-1982", "21-7-1986"],
        ["W. Kok", "21-7-1986", "5-11-1989"],
        ["Drs. M. A. M. Wöltgens", "6-11-1989", "16-5-1994"],
        ["W. Kok", "17-5-1994", "22-8-1994"],
        ["Drs. J. Wallage", "30-8-1994", "6-5-1998"],
        ["W. Kok", "7-5-1998", "14-5-1998"],
        ["Drs. J. Wallage", "14-5-1998", "10-7-1998"],
        ["Drs. A. P. W. Melkert", "10-7-1998", ""]
    ],

    'PvdV': [
        ["Dr. S. E. B. Bierema", "4-6-1946", "27-7-1948"]
    ],

    'VVD': [
        ["Mr. P. J. Oud", "27-7-1948", "6-6-1963"],
        ["R. Zeegering Hadders", "6-6-1963", "2-7-1963", "(waarnemend)"],
        ["Mr. E. H. Toxopeus", "2-7-1963", "1-9-1963"],
        ["Mr. W. J. Geertsema", "1-9-1963", "11-3-1966"],
        ["Mr. E. H. Toxopeus", "12-3-1966", "30-9-1969"],
        ["Mr. W. J. Geertsema", "1-10-1969", "5-7-1971"],
        ["H. Wiegel", "20-7-1971", "18-12-1977"],
        ["Mr. J. G. Rietkerk", "19-12-1977", "9-6-1981"],
        ["H. Wiegel", "10-6-1981", "24-8-1981", "(geen lid TK.)"],
        ["H. Wiegel", "25-8-1981", "19-4-1982"],
        ["Drs. E. H. T. M. Nijpels", "20-4-1982", "9-7-1986"],
        ["Dr. ir. J. J. C. Voorhoeve", "9-7-1986", "30-4-1990"],
        ["Mr. drs. F. Bolkestein", "1-5-1990", "30-7-1998"],
        ["H.F. Dijkstal", "30-7-1998", ""]
    ]
}

lijsttrekkers_postwar_json = []

for partijnaam, personen in lijsttrekkers_postwar.items():
    for persoon in personen:
        naam, begin, eind = persoon[0], persoon[1], persoon[2]
        ix_achternaam = naam.rfind('.')
        voorletters = naam[:ix_achternaam+1].strip()
        achternaam = naam[ix_achternaam+1:].strip()

        defdate = datetime.datetime(2000, 1, 1)

        try:
            begin_d = dateutil.parser.parse(begin, default=defdate)
        except ValueError:
            begin_d = defdate
        try:
            eind_d = dateutil.parser.parse(eind, default=defdate)
        except ValueError:
            eind_d = defdate

        lijsttrekkers_postwar_json.append({
            'voorletters': voorletters,
            'complete achternaam': achternaam,
            'partij': partijnaam,
            'begin_jaar': begin_d.year,
            'eind_jaar': eind_d.year,
            'begin_maand': begin_d.month,
            'eind_maand': eind_d.month,
            'begin_dag': begin_d.day,
            'eind_dag': eind_d.day,
            })

        if len(persoon) >= 4:
            lijsttrekkers_postwar_json[-1]['comment'] = persoon[3]

lijsttrekkers_postwar_df = pd.DataFrame(lijsttrekkers_postwar_json)

import pidilib.data.personen_lijsttrekkers_2018

lijsttrekkers_2018_postwar_df = pd.DataFrame(pidilib.data.personen_lijsttrekkers_2018.lijsttrekkers_2018_postwar_json)
