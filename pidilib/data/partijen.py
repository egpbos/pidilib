# socialists - interbellum
sdap = """Sociaal-Democratische Arbeiders Partij
SDAP
S.D.A.P.
Soc.Dem. Arbeiderspartij
Sociaal-Democratische Arbeiderspartij
S.D."""

# socialists - post-war
pvda = """Partij van de Arbeid
PvdA
P.v.d.A."""

# catholics - interbellum
rksp = """Roomsch-Katholieke Staatspartij
RKSP
R.K.S.P.
r.k.
Roomsch-Katholieke Staats-Partij
Rooms-Katholieke Staatspartij
R.K. Staatspartij"""

abrkv = """Algemeene Bond van Roomsch-Katholieke Kiesvereenigingen in Nederland
ABRKV
Algemeene Bond van R.K. Kiesvereenigingen in Nederland
Algemene Bond van Rooms-Katholieke Kiesverenigingen in Nederland
AB
ABRKKN
r.k.
Algemeene Bond van Roomsch-Katholieke Rijkskieskringorganisaties in Nederland"""

# catholics - post-war
kvp = """Katholieke Volkspartij
KVP
K.V.P.
Kath. Volkspartij"""

# liberals - interbellum
lu = """Liberale Unie
LU
Lib.
L.U."""

bvl = """Bond van Vrije Liberalen
BVL
B.V.L.
Vrije Liberalen
Oud-liberalen"""

eb = """Economische Bond
EB
E.B."""

vblsp = """Vrijheidsbond Liberale Staatspartij
Vrijheidsbond
Liberale Staatspartij
VB
V.B.
De Vrijheidsbond
Liberale Staatspartij De Vrijheidsbond
Liberale Staatspartij
LSP
L.S.P.
LSP VB
L.S.P. V.B."""

vdb = """Vrijzinnig-Democratische Bond
VDB
Vrijzinnig Democratische Bond
V.D.B.
v.d.
V.D. Bond
Vr. D."""

# liberals - post-war
pvdv = """Partij van de Vrijheid
PvdV
P.v.d.V."""

vvd = """Volkspartij voor Vrijheid en Democratie
VVD
V.V.D."""

liberalen = '''
Liberale Unie
Bond van Vrije Liberalen
Vrije Liberalen
Oud-liberalen
Economische Bond
Vrijheidsbond
Liberale Staatspartij
Vrijheidsbond
Liberale Staatspartij
LSP
L.S.P.
Vrijzinnig Democratische Bond
'''.strip()

# protestant
chu = """Christelijk-Historische Unie
CHU
C.H.U.
C.H.
Christ.-Hist. Unie
C.H. Unie
Chr.hist."""

arp = """Anti-Revolutionaire Partij
ARP
A.R.P.
A.R.     
Anti-Rev. Partij
A.R. Partij
Anti-rev."""


# v5: afkortingen met 2 letters of minder eruit
# socialists - interbellum
sdap_v5 = """Sociaal-Democratische Arbeiders Partij
SDAP
S.D.A.P.
Soc.Dem. Arbeiderspartij
Sociaal-Democratische Arbeiderspartij"""

# socialists - post-war
pvda_v5 = """Partij van de Arbeid
PvdA
P.v.d.A."""

# catholics - interbellum
rksp_v5 = """Roomsch-Katholieke Staatspartij
RKSP
R.K.S.P.
Roomsch-Katholieke Staats-Partij
Rooms-Katholieke Staatspartij
R.K. Staatspartij"""

abrkv_v5 = """Algemeene Bond van Roomsch-Katholieke Kiesvereenigingen in Nederland
ABRKV
Algemeene Bond van R.K. Kiesvereenigingen in Nederland
Algemene Bond van Rooms-Katholieke Kiesverenigingen in Nederland
ABRKKN
Algemeene Bond van Roomsch-Katholieke Rijkskieskringorganisaties in Nederland"""

# catholics - post-war
kvp_v5 = """Katholieke Volkspartij
KVP
K.V.P.
Kath. Volkspartij"""

# liberals - interbellum
lu_v5 = """Liberale Unie
Lib."""

bvl_v5 = """Bond van Vrije Liberalen
BVL
B.V.L.
Vrije Liberalen
Oud-liberalen"""

eb_v5 = """Economische Bond"""

vblsp_v5 = """Vrijheidsbond Liberale Staatspartij
Vrijheidsbond
Liberale Staatspartij
De Vrijheidsbond
Liberale Staatspartij De Vrijheidsbond
Liberale Staatspartij
LSP
L.S.P.
LSP VB
L.S.P. V.B."""

vdb_v5 = """Vrijzinnig-Democratische Bond
VDB
Vrijzinnig Democratische Bond
V.D.B.
V.D. Bond
Vr. D."""

# liberals - post-war
pvdv_v5 = """Partij van de Vrijheid
PvdV
P.v.d.V."""

vvd_v5 = """Volkspartij voor Vrijheid en Democratie
VVD
V.V.D."""

# protestant
chu_v5 = """Christelijk-Historische Unie
CHU
C.H.U.
Christ.-Hist. Unie
C.H. Unie
Chr.hist."""

arp_v5 = """Anti-Revolutionaire Partij
ARP
A.R.P.
Anti-Rev. Partij
A.R. Partij
Anti-rev."""
