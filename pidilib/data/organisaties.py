#!/usr/bin/env python
# -*- coding: utf-8 -*-

import docx
import numpy as np
import pandas as pd
import re
import copy
import os.path


df = None


def parse_year(year_cell_text, data_dict, key):
    year_search = re.search('[12][0-9]{3}', year_cell_text)
    if year_search is None:
        data_dict[key] = np.nan
        if "?" in year_cell_text:
            data_dict[key+'_zeker'] = "unknown"
        else:
            data_dict[key+'_zeker'] = "open"
    else:
        data_dict[key] = year_search.group(0)
        if "?" in year_cell_text:
            data_dict[key+'_zeker'] = "uncertain"
        else:
            data_dict[key+'_zeker'] = "certain"



def load_organisaties(dn=os.path.abspath(os.path.dirname(__file__)) + '/tabellen/organisaties_v1',
                      store_df=True):
    try:
        mislukt = []
        return pd.read_pickle(dn + '/dataframe.pickle'), mislukt
    except IOError:
        docs = {}
        docs['lib'] = docx.Document(dn + '/Organisaties Liberaal Neutraal.docx')
        docs['kat'] = docx.Document(dn + '/Organisaties Rooms-Katholiek.docx')
        docs['pro'] = docx.Document(dn + '/Organisaties Protestantse zuil.docx')
        docs['soc'] = docx.Document(dn + '/Organisaties Sociaal-Democratisch.docx')

        # tables
        tables = {}
        for zuil, doc in docs.items():
            tables[zuil] = {}
            tables[zuil]['vak/soc.econ'] = doc.tables[0]
            tables[zuil]['media'] = doc.tables[1]
            tables[zuil]['onderwijs'] = doc.tables[2]
            tables[zuil]['kerkelijk'] = doc.tables[3]
            tables[zuil]['woningbouw'] = doc.tables[4]
            tables[zuil]['zorg'] = doc.tables[5]
            tables[zuil]['cultureel'] = doc.tables[6]

        # Table specifications:
        # * Every row is one organisation
        # 
        # * Some organisations have had different names during different periods.
        #   These names are all put in the first cell. If this cell contains
        #   multiple lines, there are more than one name in it. The periods
        #   corresponding to these names are usually given on the line above the
        #   name.
        #   - Most often, it is given like "(YYYY - YYYY)". Variations include:
        #   - "(YYYY - YYYY" without a trailing paranthesis (must be typo).
        #   - "(YYYY -" if there is no known end year (assume: no end date, unless
        #     given in the end date column)
        #   - "(YYYY - )"; same as above
        #   - Without (some of the) spaces (even in front or trailing sometimes)
        #   - With a question mark, with or without parantheses, i.e.
        #     "YYYY (?)" or "YYYY ?"; signifies uncertainty, I guess.
        #   - Only a question mark "?", no year => unknown.
        #   - There are even different kinds of dashes; when there are spaces, it
        #     is usually a long "–", without spaces it is sometimes a regular 
        #     short "-".
        #   - Note: there are also some empty lines in these cells, filter these
        #     out.
        #   Given this great variation a regular expression seems the best approach
        #   to match year lines.
        # 
        # * Afkortingen and Alternatieve schrijfwijzen also often contain multiple
        #   lines. Sometimes, specific entries are even matched to specific period's
        #   name of the organisation. However, this matching is done purely
        #   visually, by aligning the entries using newlines to the entries in the
        #   name cell. Since the names in the name cell can be wrapped over multiple
        #   lines and this is dependent on markup (font, fontsize), it's going to be
        #   too much trouble to match these up properly.
        #   - By default we will just add a list for each entry in these cells to
        #     all the organisation's different period names.
        #   - For organisations with multiple names, we will add a flag to say this.
        #     This can be used to exercise caution when using the Afkortingen and
        #     Alternatieve schrijfwijzen for these organisations; they may not match
        #     the actual period's name of the organisation in question.
        #   - Note: there are also many empty lines in these cells, filter these
        #     out.
        #   - In the liberal document (not the socialist) the alternative names are
        #     sometimes separated by commas, not newlines!
        #   - In the socialist document, the alternative names are separated by
        #     newlines, but also (in most cases, not all) by semicolons. In the 
        #     liberal, there are also cells where only semicolons are used, no
        #     newlines.
        #     
        # * Begin/eind datum:
        #   - can contain question mark only -> unknown
        #   - empty -> no end
        #   - "YYYY (?)" or even "?YYYY" -> uncertain
        #
        # * Type: there is one entry which is uncertain as well "(?)".
        
        columns = ("org_id", "naam", "type", "subtype",
                   "begin", "eind", "naam_begin", "naam_eind",
                   "begin_zeker", "eind_zeker", "naam_begin_zeker",
                   "naam_eind_zeker", "subtype_zeker",
                   "afkortingen", "alt_schrijfwijzen",
                   "voortgekomen_uit", "opgegaan_in",
                   "comments")
        # naam_begin and naam_eind only used when there are multiple name periods;
        # type is the table from which it came, subtype is the type column from the
        # table
        
        df_data = []
        mislukt = []

        org_id = 0

        for zuil, zuil_tables in tables.items():
            for table_name, table in zuil_tables.items():
                # filter out the first rows (wide, merged cells and column headers):
                if table_name == "vak/soc.econ":
                    start_row = 3
                else:
                    start_row = 2

                for row in table.rows[start_row:]:
                    row_data = {}
                    row_data["zuil"] = zuil
                    row_data["org_id"] = org_id
                    org_id += 1  # increment for next organisation (= table row)

                    row_data["comments"] = ""
                    row_data["type"] = table_name

                    row_data["subtype"]          = row.cells[1].text
                    row_data["voortgekomen_uit"] = row.cells[6].text
                    row_data["opgegaan_in"]      = row.cells[7].text

                    # columns that need some processing
                    naam_col              = row.cells[0].text
                    begin_col             = row.cells[2].text
                    eind_col              = row.cells[3].text
                    afkorting_col         = row.cells[4].text
                    alt_schrijfwijzen_col = row.cells[5].text

                    # turn into list, separate by "\n", "," or ";", no empty splits
                    empty = ["", " "]
                    row_data["afkortingen"] = [i for i in re.split("[\n;]*", afkorting_col) if i not in empty]
                    row_data["alt_schrijfwijzen"] = [i for i in re.split("[\n;]*", alt_schrijfwijzen_col) if i not in empty]

                    # begin / end years parsing, including question marks and empty
                    for key, col in {'begin': begin_col, 'eind': eind_col}.items():
                        parse_year(col, row_data, key)

                    # naam_col: for every name/period a different data dict
                    if "\n" in naam_col:
                        # this re also includes newline followed by space,
                        # followed by another newline, to cope with "empty" lines:
                        namen_periodes = re.split("\n+ *\n*", naam_col)
                        namen = namen_periodes[1::2]
                        periodes = namen_periodes[::2]
                        if len(periodes) == len(namen):
                            for naam, periode in zip(namen, periodes):
                                # make dict for each name:
                                row_data_period = copy.deepcopy(row_data)
                                row_data_period['naam'] = naam
                                # periode parsing, including question marks
                                # split begin and end:
                                try:
                                    begin, eind = re.split(r"[\u2013-]", periode)
                                except:
                                    row_data_period["comments"] += "Couldn't split name period. Raw period string was: " + periode
                                    mislukt.append(row_data_period)
                                    continue
                                # and process begin and end:
                                for key, jaar_text in {'naam_begin': begin, 'naam_eind': eind}.items():
                                    parse_year(jaar_text, row_data_period, key)
                                # add completed row_data_period to df_data
                                df_data.append(row_data_period)
                        else:
                            row_data["comments"] += "Number of names doesn't match number of periodes, used full Naam cell as one name.\n"
                            row_data["naam"] = naam_col
                            # add completed row_data to df_data
                            mislukt.append(row_data)
                    else:
                        row_data["naam"] = naam_col
                        row_data["naam_begin"] = row_data["begin"]
                        row_data["naam_eind"] = row_data["eind"]
                        row_data["naam_begin_zeker"] = row_data["begin_zeker"]
                        row_data["naam_eind_zeker"] = row_data["eind_zeker"]
                        # add completed row_data to df_data
                        df_data.append(row_data)

        df = pd.DataFrame(df_data)

        if store_df:
            df.to_pickle(dn + '/dataframe.pickle')

        return df, mislukt


df, mislukt = load_organisaties()

if mislukt:
    print("Er zijn ook enkele rijen mislukt! Zie "+
          "pidilib.data.organisaties.mislukt lijst.\n")

print("N.B.: de alternatieve schrijfwijzen zijn erg moeilijk te parsen (geen "+
      "eenduidige stuctuur, vooral qua newlines), beter niet gebruiken!")
