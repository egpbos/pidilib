#!/usr/bin/env python
# -*- coding: utf-8 -*-

import docx
import os

lt = docx.Document(os.path.abspath(os.path.dirname(__file__)) + '/tabellen/lijsttrekkers KVP en PvdA.docx')

lt_groups = {}
for line in lt.paragraphs:
    set_year = False  # for getting out of for loop below
    t = line.text

    # remove crap
    if t == "\n":
        continue

    # party grouping
    if "Lijsttrekkers" in t:
        party_etc = t[t.find("Lijsttrekkers ") + 14:]
        party = party_etc[:party_etc.find(' ')]
        lt_groups[party] = {}
        continue

    # year grouping
    for run in line.runs:
        if run.bold:
            try:
                year = int(t.split()[0].strip(':'))
            except ValueError:
                year = t.split()[0].strip(':')  # string "year" ('Totaal', for instance)
            lt_groups[party][year] = []
            set_year = True
            continue
    if set_year:
        continue

    # add name
    if len(t) > 0:
        ix_voorletters_eind = t.rfind('.')
        
        lt_groups[party][year].append({'voorletters': t[:ix_voorletters_eind+1].strip().replace(' ', ''),
                                       'complete achternaam': t[ix_voorletters_eind+1:].strip()})


verkiezings_jaren = []
verkiezings_jaren_str = []
for jaren in lt_groups.values():
    for jaar in jaren:
        try:  # here it should be an int
            verkiezings_jaren.append(int(jaar))
        except ValueError:  # it's a string though
            verkiezings_jaren_str.append(jaar)
            
verkiezings_jaren = sorted(list(set(verkiezings_jaren))) + list(set(verkiezings_jaren_str))

verkiezings_data_per_jaar = {1946: {'maand': 5, 'dag': 16},
                             1948: {'maand': 7, 'dag': 7},
                             1952: {'maand': 6, 'dag': 25},
                             1956: {'maand': 6, 'dag': 13},
                             1959: {'maand': 3, 'dag': 12},
                             1963: {'maand': 5, 'dag': 15},
                             1967: {'maand': 2, 'dag': 15}}

verkiezings_query_periode_per_jaar = {}
start_ix = 1
for ix, jaar in enumerate(verkiezings_jaren[start_ix:-1]):
    j_ix = ix + start_ix
    begin_d = verkiezings_data_per_jaar[verkiezings_jaren[j_ix-1]]
    eind_d = verkiezings_data_per_jaar[jaar]
    verkiezings_query_periode_per_jaar[jaar] = {
        'begin_jaar': verkiezings_jaren[j_ix-1],
        'eind_jaar': jaar,
        'begin_maand': begin_d["maand"],
        'eind_maand': eind_d["maand"],
        'begin_dag': begin_d["dag"],
        'eind_dag': eind_d["dag"]
    }

excluded_verkiezings_query_periode_per_jaar = {}

begin_d = verkiezings_data_per_jaar[1946]
eind_d = verkiezings_data_per_jaar[1946]

excluded_verkiezings_query_periode_per_jaar[1946] = {
        'begin_jaar': 1946,
        'eind_jaar': 1946,
        'begin_maand': begin_d["maand"],
        'eind_maand': eind_d["maand"],
        'begin_dag': begin_d["dag"],
        'eind_dag': eind_d["dag"]
}

eind_d = verkiezings_data_per_jaar[1967]

excluded_verkiezings_query_periode_per_jaar['Totaal'] = {
        'begin_jaar': 1946,
        'eind_jaar': 1967,
        'begin_maand': begin_d["maand"],
        'eind_maand': eind_d["maand"],
        'begin_dag': begin_d["dag"],
        'eind_dag': eind_d["dag"]
}


lijsttrekkers_2018_postwar_json = []
lijsttrekkers_2018_1946_json = []
lijsttrekkers_2018_Totaal_json = []

for partijnaam, jaren in lt_groups.items():
    for jaar, personen in jaren.items():
        for persoon in personen:
            try:
                periode = verkiezings_query_periode_per_jaar[jaar]
                fill_this = lijsttrekkers_2018_postwar_json
            except KeyError:
                periode = excluded_verkiezings_query_periode_per_jaar[jaar]
                if jaar == 1946:
                    fill_this = lijsttrekkers_2018_1946_json
                elif jaar == 'Totaal':
                    fill_this = lijsttrekkers_2018_Totaal_json

            fill_this.append({
                'voorletters': persoon['voorletters'],
                'complete achternaam': persoon['complete achternaam'],
                'partij': partijnaam,
                'begin_jaar': periode["begin_jaar"],
                'eind_jaar': periode["eind_jaar"],
                'begin_maand': periode["begin_maand"],
                'eind_maand': periode["eind_maand"],
                'begin_dag': periode["begin_dag"],
                'eind_dag': periode["eind_dag"],
            })
