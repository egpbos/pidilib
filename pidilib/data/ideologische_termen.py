#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Every line contains comma-separated related concepts. Unrelated concepts go on
another line. A concept can consist of multiple words/terms.
"""

qual = {
    "interbellum": {
        "SDAP": """socialisme, socialistisch, socialistische 
socialisatie
democratisch socialisme, democratische socialisme
kapitaal, kapitalist, kapitalisme, kapitalistisch, kapitalistische, kapitalistische voortbrenging, kapitalistische voortbrengingswijze, kapitaalbezitter
productiemiddelen
ordening
sociaal-democratie, sociaal-democratisch, sociaal-democratische
tegenstelling, tegenstellingen
bestaansonzekerheid
uitbuiting, uitgebuiten, uitbuiters
klasse, arbeidersklasse, bezittende klasse
klassenstrijd
proletariaat
arbeiders
plan, planning, planmatig
gemeenschap, gemeenschapsorganen""",
        "ARP": """Anti-revolutionair, Anti-revolutionaire
Christelijk-historisch, Christelijk-historische
God, Gods
Gods Woord
Gratie Gods
dienares Gods, dienaresse Gods
Vrije School
organisch
gezinshoofd, gezinshoofden
overheid
godsdienstig, godsdienstige
beginsel, beginselen
ordinantiën, ordinantiën Gods
kerk, kerken
grondwet
natie 
volkskarakter
Oranje
zeden, zedelijk, zedelijke, onzedelijk, onzedelijke""",
        "CHU": """Christelijk-historisch, Christelijk-historische
Christelijk, Christelijke
God, Gods
overheid
gezag
ordening Gods
kerk, kerken
beginsel, beginselen
volksleven
zeden, zedelijk, zedelijke, onzedelijk, onzedelijke, zedelijkheid
openbaring
dienares Gods, dienaresse Gods
H. Schrift, Heilige Schrift
majoriteit
autoriteit
Christelijke staat in Protestantschen zin
Oranje
bijzondere school
zondagsrust""",
        "VDB": """overheid
volksvertegenwoordiging
medezeggenschap
vrijzinnig-democratische
democratisch, democratische
vrije georganiseerde wilsuiting
openbaarheid
maatschappelijke ontwikkelingsvoorwaarden, gelijkmaking van maatschappelijke ontwikkelingsvoorwaarden
staatkundige opvoeding, staatkundige opvoeding des volks
persoonlijk eigendomsrecht, erkenning van persoonlijk eigendomsrecht
sociale maatregelen
leidende taak, leidende taak overheid
vrucht van eigen inspanning, aan ieder vrucht van eigen inspanning verzekeren
persoonlijke vrijheid
geestelijke  vrijheid""",
        "LSP": """liberalisme, liberaal
menschelijke persoonlijkheid, erkenning menschelijke persoonlijkheid
vrijheid, vrijheidsbeginsel
persoonlijke vrijheid
geestelijke vrijheid
maatschappelijke vrijheid
volkseenheid
vrijhandel
volkskracht, verhooging van de volkskracht
particulier initiatief
ontplooiing
verantwoordelijkheid
burgerzin
clericalisme
overheidsingrijpen, overheidsingrijpen in negatieve zin
gemeenschap""",
        # liberal merger, mailed by Gerrit Dec 9 2015:
        "lib":"""medezeggenschap
vrijzinnig-democratisch
democratisch, democratische
vrije georganiseerde wilsuiting
vrijheidsbeginsel
openbaarheid
gelijkmaking van maatschappelijke ontwikkelingsvoorwaarden
maatschappelijke ontwikkelingsvoorwaarden
staatkundige opvoeding
persoonlijk eigendomsrecht
persoonlijke vrijheid
geestelijke vrijheid
liberalisme, liberaal
menschelijke persoonlijkheid
menselijke persoonlijkheid
maatschappelijke vrijheid
volkseenheid
vrijhandel
volkskracht
particulier initiatief
ontplooiing
verantwoordelijkheid
burgerzin
clericalisme
gemeenschap""",
        "RKSP": """Christelijke beginselen
positief-christelijk
Z.H. den paus, den paus, paus
Gezantschapbij den H. Stoel
Quadragesimo Anno
sociale liefde
ordening
solidariteit, solidariteitsgedachte
organisch, organische samenwerking
harmonie
bijzondere school, bijzonder onderwijs
grootte van het gezin, behoefte van groote gezinnen
missie, missiën, missie-arbeid, missionaris, missionarissen
gezin, natuurlijke gemeenschap
zondagsrust
bioscoop- en schouwburggevaar
eigen woning
huwelijk""",
        "lib_optimal":"""vrije georganiseerde wilsuiting
maatschappelijke ontwikkelingsvoorwaarden
staatkundige opvoeding
persoonlijk eigendomsrecht
menschelijke persoonlijkheid
maatschappelijke vrijheid
vrijhandel
volkskracht
particulier initiatief
ontplooiing
verantwoordelijkheid
burgerzin""",
    }
}

qual["wederopbouw"] = {
        "CHU": """Christelijk-historisch, Christelijk-historische
Christelijk, Christelijke
God, Gods
overheid
gezag
ordening Gods
kerk, kerken
beginsel, beginselen
volksleven
zeden, zedelijk, zedelijke, onzedelijk, onzedelijke, zedelijkheid
openbaring
dienares Gods, dienaresse Gods
H. Schrift, Heilige Schrift
majoriteit
autoriteit
Christelijke staat in Protestantschen zin
Oranje
bijzondere school
zondagsrust""",

"VVD":
"""individuele mens, individuele geweten 
sociale gerechtigheid
liberaal, liberale, liberalisme
verdraagzaamheid, verdraagzaamheid tegenover andersdenkenden
democratie 
middengroepen
humanisme
eigen werkzaamheid
ondernemingsgewijze produktie
vrijheid, vrijheden, ware vrijheid
vrije persoonlijkheid, vrijheid van de mens
geestelijke vrijheid
maatschappelijke vrijheid
staatkundige vrijheid
vrije maatschappelijke leven
zelfstandige kracht in het volk
particulier initiatief, particuliere initiatief
eigenbelang
eigen eigendomsrecht, private eigendomsrecht, particuliere eigendomsrecht
particuliere bedrijfsvorm
ontplooiing
verantwoordelijkheid, verantwoordelijkheidsbesef, eigen verantwoordelijkheid
overheidsingrijpen, overheidsbemoeiing
gemeenschap""",

"PvdA":
"""rechtvaardig, rechtvaardige, rechtvaardigheid, sociale rechtvaardigheid
sociale gerechtigheid 
bestaanszekerheid, bestaansonzekerheid
volkswelvaart
geestelijke vrijheid 
wereldvrede
rechtsorde van de arbeid
gelijke sociale ontwikkelingskansen, gelijkheid van ontwikkelingskansen
solidariteit, menselijke solidariteit 
socialisme, socialistisch, socialistische
socialisatie,  socialisatie, gesocialiseerd
democratisch socialisme, democratische socialisme
kapitalisme, kapitalistisch
productiemiddelen, produktiemiddelen 
ordening
sociaal-democratie, sociaal-democratisch, sociaal-democratische
klassetegenstellingen, klassentegenstellingen 
uitbuiting, uitgebuiten, uitbuiters 
arbeiders
planmatig gemeenschap, planmatig gemeenschapsorganen""",

"ARP":
"""Anti-revolutionair, Anti-revolutionaire
Christelijk-historisch, Christelijk-historische
God, Gods
Gods Woord
Gratie
dienares Gods, dienares van God
Vrije School
overheid
godsdienstig, godsdienstige 
beginsel, beginselen 
kerk, kerken 
grondwet
natie 
volkskarakter 
Oranje
zeden, zedelijk, zedelijke, onzedelijk, onzedelijke""",

"KVP":
"""natuurlijke zedewet
goddelijke openbaring
kerkelijk leergezag
God, ordening Gods
gezin 
huwelijk
functionele gemeenschappen
bijzonder onderwijs
publiekrechtelijke bedrijfslichamen
subsidiariteit""",

"KVP_optimal":
"""natuurlijke zedewet
goddelijke openbaring
kerkelijk leergezag
God, ordening Gods
functionele gemeenschappen
bijzonder onderwijs
publiekrechtelijke bedrijfslichamen
subsidiariteit"""

}