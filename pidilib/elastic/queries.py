import copy

zuilen_kranten_filters = {
    'kat': {
        "bool" : {
            "should" : [
                { "terms" :
                    {"paper_dc_identifier" :
                        ["832688045", "833066226", "832369047", "83245351X",
                         "832453390", "842127143", "842126635"]
                    }
                }, 
                { "terms" :
                    {"paper_dc_title_raw" :
                        ["Leidsche Courant", "De Gelderlander"]
                    }
                } 
            ],
        }
    },
    'pro': {
        "terms": {
            "paper_dc_identifier": ["832667536","810209039"]
        }
    },
    'soc': {
        "terms": {
            "paper_dc_identifier": ["850279356", "331833263", "83308013X", "841201854",
                                    # "832698679", "850161290", "832727143", "850280184"]
                                    "832698679", "850161290", "832737143", "850280184"]  # ID typo fix
        }
    },
    'lib': {
        "bool" : {
            "should" : [
                { "terms" :
                    {"paper_dc_identifier" :
                        ["852115210", "82000541X", "852121741", "833050346", "832337900",
                         "832495182", "833050788", "83249562X", "832401439", "832564818",
                         "83249531X", "833013246", "865061483", "832675288", "832689858"]
                    }
                }, 
                { "terms" :
                    {"paper_dc_title_raw" :
                        ["Het Leidsch Dagblad", "Haarlems Dagblad"]
                    }
                } 
            ],
        }
    }
}

zuilen_kranten_filters['neutraal'] = {
    "bool" : {
        "must_not" : [
            { "terms" :
                {"paper_dc_identifier" :
                    zuilen_kranten_filters['kat']['bool']['should'][0]['terms']['paper_dc_identifier'] + \
                    zuilen_kranten_filters['pro']['terms']['paper_dc_identifier'] + \
                    zuilen_kranten_filters['soc']['terms']['paper_dc_identifier'] + \
                    zuilen_kranten_filters['lib']['bool']['should'][0]['terms']['paper_dc_identifier']
                }
            }, 
            { "terms" :
                {"paper_dc_title_raw" :
                    zuilen_kranten_filters['kat']['bool']['should'][1]['terms']['paper_dc_title_raw'] + \
                    zuilen_kranten_filters['lib']['bool']['should'][1]['terms']['paper_dc_title_raw']
                }
            } 
        ],
    }
}

# for reference in old scripts that were trying to find this error:
zuilen_kranten_filters_old = copy.deepcopy(zuilen_kranten_filters)
zuilen_kranten_filters_old['soc']['terms']["paper_dc_identifier"] = ["850279356",
    "331833263", "83308013X", "841201854", "832698679", "850161290", "832727143",
    "850280184"]

zuilen_filters = {
    'kat': {
        "term": {
            "paper_zuil": "katholiek"
        }
    },
    'pro': {
        "term": {
            "paper_zuil": "protestants"
        }
    },
    'soc': {
        "term": {
            "paper_zuil": "socialistisch"
        }
    },
    'lib': {
        "term": {
            "paper_zuil": "liberaal"
        }
    },
    'neutraal': {
        "missing": {
            "field": "paper_zuil"
        }
    }
}

zuilen_kranten_filters_v2 = {
    'kat': {
        "bool" : {
            "should" : [
                { "terms" :
                    {"paper_dc_identifier" :
                        ["832688045", "833066226", "832369047", "83245351X",
                         "832453390", "842127143", "842126635"]
                    }
                }, 
                { "terms" :
                    {"paper_dc_title_raw" :
                        ["Leidsche Courant", "De Gelderlander"]
                    }
                } 
            ],
        }
    },
    'pro': {
        "terms": {
            "paper_dc_identifier": ["832667536", # De Standaard (1941, 42)
                                    "810209039", # Nederlands dagblad (67-94)
                                    "822630966"] # Geref. dagblad (48-67)
        }
    },
    'soc': {
        "terms": {
            "paper_dc_identifier": ["850279356", "331833263", "83308013X",
                                    "832698679", "850161290", "832737143",
                                    "850280184"]
        }
    },
    'lib': {
        "terms" : {
            "paper_dc_identifier" : ["82000541X", "832337900", "832495182",
                                     "832689858"]
        }
    },
    'neu': {
        "bool" : {
            "should" : [
                { "terms" :
                    {"paper_dc_identifier" :
                        ["852115210", "852121741", "833050346",
                         "833050788", "83249562X", "832401439", "832564818",
                         "83249531X", "833013246", "865061483", "832675288"]
                    }
                }, 
                { "terms" :
                    {"paper_dc_title_raw" :
                        ["Het Leidsch Dagblad", "Haarlems Dagblad"]
                    }
                } 
            ],
        }
    }
}

zuilen_kranten_filters_v2['rest'] = {
    "bool" : {
        "must_not" : [
            { "terms" :
                {"paper_dc_identifier" :
                    zuilen_kranten_filters_v2['kat']['bool']['should'][0]['terms']['paper_dc_identifier'] + \
                    zuilen_kranten_filters_v2['pro']['terms']['paper_dc_identifier'] + \
                    zuilen_kranten_filters_v2['soc']['terms']['paper_dc_identifier'] + \
                    zuilen_kranten_filters_v2['lib']['terms']['paper_dc_identifier'] + \
                    zuilen_kranten_filters_v2['neu']['bool']['should'][0]['terms']['paper_dc_identifier']
                }
            }, 
            { "terms" :
                {"paper_dc_title_raw" :
                    zuilen_kranten_filters_v2['kat']['bool']['should'][1]['terms']['paper_dc_title_raw'] + \
                    zuilen_kranten_filters_v2['neu']['bool']['should'][1]['terms']['paper_dc_title_raw']
                }
            } 
        ],
    }
}

zuilen_kranten_filters_v2_extra = {
    'com': {
        "terms": {
            "paper_dc_identifier": ["841201854"]
        }
    },
}




zuilen_kranten_filters_v3 = {
    'kat': {
        "bool" : {
            "should" : [
                { "terms" :
                    {"paper_dc_identifier" :
                        ["832688045", "833066226", "832369047", "83245351X",
                         "832453390", "842127143", "842126635"]
                    }
                }, 
                { "terms" :
                    {"paper_dc_title_raw" :
                        ["Leidsche Courant", "De Gelderlander"]
                    }
                } 
            ],
        }
    },
    'pro': {
        "terms": {
            "paper_dc_identifier": ["832667536",] # De Standaard (1941, 42)
        }
    },
    'soc': {
        "terms": {
            "paper_dc_identifier": ["850279356", "331833263", "83308013X",
                                    "832698679", "850161290", "832737143",
                                    "850280184"]
        }
    },
    'lib': {
        "terms" : {
            "paper_dc_identifier" : ["82000541X", "832337900", "832495182",
                                     "832689858"]
        }
    },
    'neu': {
        "bool" : {
            "should" : [
                { "terms" :
                    {"paper_dc_identifier" :
                        ["852115210", "852121741", "833050346",
                         "833050788", "83249562X", "832401439", "832564818",
                         "83249531X", "833013246", "865061483", "832675288"]
                    }
                }, 
                { "terms" :
                    {"paper_dc_title_raw" :
                        ["Het Leidsch Dagblad", "Haarlems Dagblad"]
                    }
                } 
            ],
        }
    }
}

zuilen_kranten_filters_v3['rest'] = {
    "bool" : {
        "must_not" : [
            { "terms" :
                {"paper_dc_identifier" :
                    zuilen_kranten_filters_v3['kat']['bool']['should'][0]['terms']['paper_dc_identifier'] + \
                    zuilen_kranten_filters_v3['pro']['terms']['paper_dc_identifier'] + \
                    zuilen_kranten_filters_v3['soc']['terms']['paper_dc_identifier'] + \
                    zuilen_kranten_filters_v3['lib']['terms']['paper_dc_identifier'] + \
                    zuilen_kranten_filters_v3['neu']['bool']['should'][0]['terms']['paper_dc_identifier']
                }
            }, 
            { "terms" :
                {"paper_dc_title_raw" :
                    zuilen_kranten_filters_v3['kat']['bool']['should'][1]['terms']['paper_dc_title_raw'] + \
                    zuilen_kranten_filters_v3['neu']['bool']['should'][1]['terms']['paper_dc_title_raw']
                }
            } 
        ],
    }
}


zuilen_kranten_filters_v3_extra = {
    'com': {
        "terms": {
            "paper_dc_identifier": ["841201854"]
        }
    },
    'ref': {
        "terms": {
            "paper_dc_identifier": ["810209039", # Nederlands dagblad (67-94)
                                    "822630966"] # Geref. dagblad (48-67)
        }
    }
}





zuilen_kranten_filters_v4 = {
    'kat': {
        "bool" : {
            "should" : [
                { "terms" :
                    {"paper_dc_identifier" :
                        ["832688045", "833066226", "832369047", "83245351X",
                         "832453390", "842127143", "842126635"]
                    }
                }, 
                { "terms" :
                    {"paper_dc_title_raw" :
                        ["Leidsche Courant", "De Gelderlander"]
                    }
                } 
            ],
        }
    },
    'pro': {
        "terms": {
            "paper_dc_identifier": ["832667536",] # De Standaard (1941, 42)
        }
    },
    'soc': {
        "terms": {
            "paper_dc_identifier": ["850279356", "331833263", "83308013X",
                                    "832698679", "850161290", "832737143",
                                    "850280184"]
        }
    },
    'lib': {
        "terms" : {
            "paper_dc_identifier" : ["82000541X", "832337900", "832495182",
                                     "832689858"]
        }
    },
    'neu': {
        "bool" : {
            "should" : [
                { "terms" :
                    {"paper_dc_identifier" :
                        ["852115210", "852121741", "833050346",
                         "833050788", "83249562X", "832401439", "832564818",
                         "83249531X", "833013246", "865061483", "832675288"]
                    }
                }, 
                { "terms" :
                    {"paper_dc_title_raw" :
                        ["Het Leidsch Dagblad", "Haarlems Dagblad"]
                    }
                } 
            ],
        }
    }
}

# Leave out rest so that it does not contaminate the total (discussed 9 dec 2015):
# zuilen_kranten_filters_v4['rest'] = {
#     "bool" : {
#         "must_not" : [
#             { "terms" :
#                 {"paper_dc_identifier" :
#                     zuilen_kranten_filters_v3['kat']['bool']['should'][0]['terms']['paper_dc_identifier'] + \
#                     zuilen_kranten_filters_v3['pro']['terms']['paper_dc_identifier'] + \
#                     zuilen_kranten_filters_v3['soc']['terms']['paper_dc_identifier'] + \
#                     zuilen_kranten_filters_v3['lib']['terms']['paper_dc_identifier'] + \
#                     zuilen_kranten_filters_v3['neu']['bool']['should'][0]['terms']['paper_dc_identifier']
#                 }
#             }, 
#             { "terms" :
#                 {"paper_dc_title_raw" :
#                     zuilen_kranten_filters_v3['kat']['bool']['should'][1]['terms']['paper_dc_title_raw'] + \
#                     zuilen_kranten_filters_v3['neu']['bool']['should'][1]['terms']['paper_dc_title_raw']
#                 }
#             } 
#         ],
#     }
# }


zuilen_kranten_filters_v4_extra = {
    'com': {
        "terms": {
            "paper_dc_identifier": ["841201854"]
        }
    },
    'ref': {
        "terms": {
            "paper_dc_identifier": ["810209039", # Nederlands dagblad (67-94)
                                    "822630966"] # Geref. dagblad (48-67)
        }
    }
}


zuilen_kranten_filters_v5 = copy.deepcopy(zuilen_kranten_filters_v4)
zuilen_kranten_filters_v5['soc']["terms"]["paper_dc_identifier"].remove("331833263")




def date_range_filter(gte, lte):
    if isinstance(lte, int):
        lte = str(lte)
    if isinstance(gte, int):
        gte = str(gte)
    return {
        "range": {
            "paper_dc_date": {
                "gte": gte,
                "lte": lte
            }
        }
    }

def and_filter(filter_list):
    return {
        "bool": {
            "must": filter_list
        }
    }

def qsq_query_from_phrases(phrases_str):
    """Give phrases as a string; each phrase should be on its own line.
    Returns query string query query-string (containing AND's and OR's and
    parentheses).
    Replaces slashes (/) with AND, since ES does not like slashes."""
    qsq_query = ""
    phrases = phrases_str.split('\n')
    for phrase in phrases[:-1]:
        qsq_query += "(" + " AND ".join(phrase.split()) + ") OR "
    qsq_query += "(" + " AND ".join(phrases[-1].split()) + ")"

    qsq_query = qsq_query.replace("/", " AND ")

    return qsq_query


def querystr_from_igterm_line(line):
    """
    Returns query string query query-string (containing AND's and OR's and
    parentheses). Also puts quotation marks around the concepts to search
    for the literal combination, not just the co-occurring words (tip from
    larsmans).
    """
    concepts = [x.strip() for x in line.split(',')]
    return "\"" + "\" OR \"".join(concepts) + "\""


def list_querystrs_from_igtermen_phrases(phrases):
    return [querystr_from_igterm_line(line) for line in phrases.split('\n')]


def querystr_from_igtermen_phrases(phrases):
    return " OR ".join(list_querystrs_from_igtermen_phrases(phrases))


def phrases_str_from_organisations(org_df, incl_afk=False, incl_alt=False):
    out_str = ""
    for ix, o in org_df.iterrows():
        out_str += o['naam'] + "\n"
        if incl_afk:
            for afk in o['afkortingen']:
                out_str += afk + "\n"
        if incl_alt:
            for alt in o['alt_schrijfwijzen']:
                out_str += alt + "\n"
    out_str = out_str[:-1]  # remove last new-line
    return out_str


def phrases_str_from_persons(pers_df, include_party=True):
    out_str = ""
    for ix, p in pers_df.iterrows():
        out_str += " ".join([p['voorletters'], p['complete achternaam']]) + "\n"
        out_str += " ".join([p['complete achternaam']]) + "\n"
        if include_party:
            out_str += " ".join([p['voorletters'], p['complete achternaam'], p['partij']]) + "\n"
            out_str += " ".join([p['complete achternaam'], p['partij']]) + "\n"
        if p['roepnaam']:
            try:
                out_str += " ".join([p['roepnaam'], p['complete achternaam']]) + "\n"
                if include_party:
                    out_str += " ".join([p['roepnaam'], p['complete achternaam'], p['partij']]) + "\n"
            except TypeError:
                print(p)
    out_str = out_str[:-1]  # remove last new-line
    return out_str


def phrases_str_from_persons_simple(pers_df, include_party=True):
    out_str = ""
    for ix, p in pers_df.iterrows():
        # out_str += " ".join([p['voorletters'], p['complete achternaam']]) + "\n"
        out_str += " ".join([p['complete achternaam']]) + "\n"
        if include_party:
            # out_str += " ".join([p['voorletters'], p['complete achternaam'], p['partij']]) + "\n"
            out_str += " ".join([p['complete achternaam'], p['partij']]) + "\n"
        # if p['roepnaam']:
        #     try:
        #         out_str += " ".join([p['roepnaam'], p['complete achternaam']]) + "\n"
        #         if include_party:
        #             out_str += " ".join([p['roepnaam'], p['complete achternaam'], p['partij']]) + "\n"
        #     except TypeError:
        #         print(p)
    out_str = out_str[:-1]  # remove last new-line
    return out_str


def phrases_str_from_lijsttrekkers_partij(lt_df):
    lt_str = ""
    for ix, p in lt_df.iterrows():
        lt_str += " ".join([p['voorletters'], p['complete achternaam'], p['partij']]) + "\n"
        lt_str += " ".join([p['complete achternaam'], p['partij']]) + "\n"
        if p['roepnaam']:
            lt_str += " ".join([p['roepnaam'], p['complete achternaam'], p['partij']]) + "\n"
    lt_str = lt_str[:-1]  # remove last new-line
    return lt_str

#
# Query body objects
#

arts_per_yr_agg = { "articles_per_year" : { "date_histogram" : {
            "field" : "paper_dc_date", "interval" : "year" } } }
arts_per_mn_agg = { "articles_per_year" : { "date_histogram" : {
            "field" : "paper_dc_date", "interval" : "month" } } }
paper_names_agg = { "newspaper names" : { "terms" : { "field" : "paper_dc_title_raw", "size": 0 } } }
# size 0: default number of returned buckets for terms agg is the top 10 buckets; 0 gives them all
paper_arts_per_yr_agg = { "newspaper names" :
    {
    "terms" :
        {
        "field" : "paper_dc_title_raw",
        "size": 0
        },
    "aggs" : arts_per_yr_agg
    }
}

all_query = { "match_all" : { } }


def query_string_query(query):
    return {"query_string": {"default_field": "text_content", "query": query}}


def filtered_query(query, filtr):
    return {"bool": {"must": query, "filter": filtr}}


def terms_filter(field_name, terms_list):
    return {"terms": {field_name: terms_list}}
