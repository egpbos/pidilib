#!/usr/bin/env python3

"""
Clear cache that was loaded using memdrive_cache.py

Remove the file in the cache and the symlink and rename the original file back
to its original name (remove the _bak suffix).
"""
import glob
import shutil
import os

data_dn = "/data/staging/processed/"
cache_dn = "/mnt/ramdisk/"

cachelist = sorted(glob.glob(cache_dn + "*.data.gz"))

fnlist = [s.split('/')[-1] for s in cachelist]

for fn in fnlist:
    os.remove(data_dn + fn)  # remove symlink
    os.rename(data_dn + fn + "_bak", data_dn + fn)  # rename original file
    os.remove(cache_dn + fn)  # remove cached file
