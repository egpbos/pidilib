#!/usr/bin/env python3

"""
Use a memdrive (or other fast drive) cache to load the files from.

First check which files have already been processed and then make a selection
of a certain number of files to copy to the memdrive and make a softlink to
from the original location. The original file is renamed, appending _bak to its
filename, so that you don't need to copy the file back later, you can just
remove the link and the memdrive copied file and give the original file its
original name.
"""
import glob
import shutil
import os

data_dn = "/data/staging/processed/"
progress_dn = "/home/patrick/pidilib/elasticsearch_import/kb_pidimehs_progress/"

cache_dn = "/mnt/ramdisk/"

number = 500

filelist = sorted(glob.glob(data_dn + "*.data.gz"))
progresslist = glob.glob(progress_dn + "*.log")

fnlist = [s.split('/')[-1] for s in filelist]
fnprogresslist = [s.split('/')[-1] for s in progresslist]

unprocessedfnlist = [fn for fn in fnlist if fn.replace('.gz', '.log') not in fnprogresslist]

for fn in unprocessedfnlist[:number]:
    shutil.copy(data_dn + fn, cache_dn)
    os.rename(data_dn + fn, data_dn + fn + "_bak")
    os.symlink(cache_dn + fn, data_dn + fn)
