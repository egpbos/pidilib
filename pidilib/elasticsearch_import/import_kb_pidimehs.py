#!/usr/bin/env python3

import xml.etree.cElementTree as ElementTree
import gzip
import os
import sys
import elasticsearch
from elasticsearch import helpers
import datetime
import glob
import tqdm
import collections
import time  # for waiting until elasticsearch is connected
import json
import argparse
import getpass
import concurrent.futures


faillog_file = open('faillog_%i.txt' % time.time(), 'w')


def jprint(dictje):
    print(json.dumps(dictje, sort_keys=True, indent=3))


def encode_str(text, errors="ignore"):
    """Options for errors: ignore, which leaves out the character;
                           backslashreplace."""
    # return text.encode('ascii', errors=errors)
    return text


def index_document(doc_obj, _id, INDEX_NAME, es, verbose=False):
    retries = 1
    retried = 0
    while True and retried <= retries:
        try:
            # if retried > 0:
                # print(es.index(INDEX_NAME, "doc", doc_obj, id=_id, request_timeout=60))
            # else:
                return es.index(INDEX_NAME, "doc", doc_obj, id=_id), len(doc_obj['text_content'])
            # time.sleep(1)  Dit werkt niet
        except (elasticsearch.ConnectionTimeout, # this is what I get @ HPC cloud
                elasticsearch.exceptions.ConnectionError): # this I get on laptop 
            if retried < retries:
                print("\nconnection timeout while indexing doc with id", _id, "retrying with timeout 60...")
                # time.sleep(5)
                retried += 1
                continue
            else:
                print("doc", _id, "not indexable, skipping")
                faillog_file.write(json.dumps(doc_obj, sort_keys=True, indent=3) + "\n\n")
                break
        break


def write_progress(logfile, num_total, num_indexed, num_filtered, success=True):
    date_str = str(datetime.datetime.now())

    # check for progress-log directory (create if non-existent)
    dirname = os.path.dirname(logfile)
    if not os.path.exists(dirname):
        os.mkdir(dirname)

    f = open(logfile, 'w')
    if not success: f.write('FAILED!\n\n')
    f.write(date_str + "\n")
    f.write("total documents in file: " + str(num_total) + "\n")
    f.write("indexed documents: " + str(num_indexed) + "\n")
    f.write("filtered documents: " + str(num_filtered) + "\n")
    f.close()


def doc_from_processed_kb_xml_elem(doc_el, full=False):
    """
    full: add all metadata, also the stuff that is not necessary for Texcavator
    """
    doc_obj = {}
    # ### Necessary for Texcavator ### #
    # paper metadata
    doc_obj['paper_dc_title'] = encode_str(doc_el.find("field[@name='dc_title']").text)
    doc_obj['paper_dc_identifier'] = encode_str(doc_el.find("field[@name='dc_identifier']").text)
    doc_obj['paper_dc_date'] = encode_str(doc_el.find("field[@name='dc_date']").text)
    doc_obj['paper_dcterms_temporal'] = encode_str(doc_el.find("field[@name='dcterms_temporal']").text)
    doc_obj['paper_dcterms_spatial'] = encode_str(doc_el.find("field[@name='dcterms_spatial']").text)

    # article metadata
    doc_obj['article_dc_subject'] = encode_str(doc_el.find("field[@name='dc_subject']").text)
    doc_obj['article_dc_title'] = encode_str(doc_el.findall("field[@name='dc_title']")[1].text)

    # text content
    content_el = doc_el.find("field[@name='content']")
    text_el = content_el.find("text") #sometimes no text_el found (invalid ocr xml)
    if text_el is None:
        doc_obj['text_content'] = ''
    else:
        try:
            text_content = "\n\n".join(el.text for el in text_el.findall("p") if el.text)
        except:
            print(ElementTree.tostring(text_el))
            raise

        # Unescape HTML entities
        text_content = text_content.replace("&amp;amp;", '&')
        text_content = text_content.replace("&amp;quot;", '"')
        text_content = text_content.replace("&amp;gt;", '>')
        text_content = text_content.replace("&amp;lt;", '<')
        text_content = text_content.replace("&amp;apos;", "'")

        doc_obj['text_content'] = encode_str(text_content)

    doc_obj['identifier'] = encode_str(doc_el.findall("field[@name='dcx_recordIdentifier']")[1].text)
    # ### Extra stuff, not needed for Texcavator ### #
    # title_raw is not needed for Texcavator, but it is for PIDIMEHS
    doc_obj['paper_dc_title_raw'] = doc_obj['paper_dc_title']
    # the stuff below is never really needed (in PIDIMEHS at least)
    if full:
        doc_obj['paper_dcterms_alternative'] = encode_str(doc_el.find("field[@name='dcterms_alternative']").text)
        doc_obj['paper_dcterms_isVersionOf'] = encode_str(doc_el.find("field[@name='dcterms_isVersionOf']").text)
        doc_obj['paper_dcx_recordRights'] = encode_str(doc_el.find("field[@name='dcx_recordRights']").text)
        doc_obj['paper_dc_publisher'] = encode_str(doc_el.find("field[@name='dc_publisher']").text)
        doc_obj['paper_dc_source'] = encode_str(doc_el.find("field[@name='dc_source']").text)
        doc_obj['paper_dcx_volume'] = encode_str(doc_el.find("field[@name='dcx_volume']").text)
        doc_obj['paper_dcx_issuenumber'] = encode_str(doc_el.find("field[@name='dcx_issuenumber']").text)
        doc_obj['paper_dcx_recordIdentifier'] = encode_str(doc_el.find("field[@name='dcx_recordIdentifier']").text)
        doc_obj['paper_dc_identifier_resolver'] = encode_str(doc_el.find("field[@name='dc_identifier_resolver']").text)
        doc_obj['paper_dc_language'] = encode_str(doc_el.find("field[@name='dc_language']").text)
        doc_obj['paper_dcterms_isPartOf'] = encode_str(doc_el.find("field[@name='dcterms_isPartOf']").text)
        doc_obj['paper_ddd_yearsDigitized'] = encode_str(doc_el.find("field[@name='ddd_yearsDigitized']").text)
        doc_obj['paper_dcterms_spatial_creation'] = encode_str(doc_el.find("field[@name='dcterms_spatial_creation']").text)
        doc_obj['paper_dcterms_issued'] = encode_str(doc_el.find("field[@name='dcterms_issued']").text)
        doc_obj['article_dcterms_accessRights'] = encode_str(doc_el.find("field[@name='dcterms_accessRights']").text)
        doc_obj['article_dcx_recordIdentifier'] = doc_obj['identifier']
        doc_obj['article_dc_identifier_resolver'] = encode_str(doc_el.findall("field[@name='dc_identifier_resolver']")[1].text)


    return doc_obj



def validate_filters(filters):
    """
    Function that checks if filters in a set are proper, i.e. not inconsistent.
    """
    # check if there are equal include and exclude filters
    for f1 in filters:
        for f2 in filters:
            if f1['include'] == (not f2['include']):  # note: "== not" != "!=" ;)
                equal = False
                keys = [k for k in list(f1.keys()) if k != "include"]
                try:
                    equal = all([f1[key] == f2[key] for key in keys])
                except KeyError:
                    continue
                if equal:
                    return False
    return True


def check_filter(doc_obj, filt):
    if   filt['type'] == 'in':
        if doc_obj[filt['field']] in filt['list']:
            return True
        else:
            return False
    elif filt['type'] == 'equals':
        if doc_obj[filt['field']] == filt['value']:
            return True
        else:
            return False
    elif filt['type'] == 'before':
        doc_date = datetime.datetime.strptime(doc_obj['paper_dc_date'], "%Y-%m-%d")
        filt_date = datetime.datetime.strptime(filt['date'], "%Y-%m-%d")
        if doc_date < filt_date:
            return True
        else:
            return False
    elif filt['type'] == 'after':
        doc_date = datetime.datetime.strptime(doc_obj['paper_dc_date'], "%Y-%m-%d")
        filt_date = datetime.datetime.strptime(filt['date'], "%Y-%m-%d")
        if doc_date > filt_date:
            return True
        else:
            return False


class Memory(object):
    filter_warning_showed = False

def passes_filters(doc_obj, filters):
    """
    Returns True if the doc_obj passes the filters, False if it does not.
    """
    if not Memory.filter_warning_showed:
        print("\n\nFilteren kan ook in ES zelf bij aggregations! Zie http://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-terms-aggregation.html#_filtering_values\n")
        Memory.filter_warning_showed = True
    if filters:  # filters can also be None or empty list
        results = {
            "include": [],
            "exclude": []
        }

        for filt in filters:
            if filt['include']:  # including filters:
                results["include"].append(check_filter(doc_obj, filt))
            else:  # excluding filters:
                results["exclude"].append(check_filter(doc_obj, filt))

        # - exclude filters act like an eraser; they only act locally. As such
        #   they can override inclusion of individual doc_objs, also if they are
        #   explicitly included by an include filter.
        # if len(results["exclude"]) > 0 and np.any(results["exclude"]):
        if any(results["exclude"]):  # needn't check > 0, any([]) is False
            return False
        # - include filters act globally; when any is used everything that is
        #   not included is automatically excluded
        if all(results["include"]):  # needn't check > 0, all([]) is True
            return True
        else:
            return False

    else:  # empty filters list or None
        return True


def enrich_doc_obj(doc_obj, enrichments):
    """
    Enriches the doc_obj by 
    """
    for enrich in enrichments:
        if passes_filters(doc_obj, enrich['filter']):
            enrichment = enrich['enrichment']
            if enrichment['type'] == 'add_field':
                doc_obj[enrichment['field_name']] = enrichment['value']


def ingest_docs_from_processed_kb_zip_file(es, zipfilename, input_dir, INDEX_NAME, log_dir, filters=None, enrichments=[], verbose=False, bulk=False):
    if not validate_filters(filters):
        raise SystemExit("filters in ingest_docs_from_processed_kb_zip_file not valid!")

    filename = zipfilename.split("/")[-1]
    if verbose: print("Processing: " + filename)
    logfile = log_dir + '/' + filename.replace('.gz', '.log')
    if os.path.exists(logfile):
        if verbose: print("... [%s] File already imported." % logfile)
        return 1
    else:
        if verbose: print("... checked progress file %s" % logfile)

    article_count = 0
    filtered_count = 0
    bulk_docs = []
    try:
        with gzip.open(zipfilename, 'rt') as zipfile:
            txt = zipfile.read()
            txt = txt.replace('&','&amp;') # quick and dirty solution to encoding entities
            etree = ElementTree.fromstring(txt)
            docs = etree.findall('doc')
            num_docs = len(docs)
            for ix_doc, doc_el in enumerate(docs):
                if verbose:
                    sys.stdout.write('... processing doc %i/%i\r' % (ix_doc, num_docs))
                    sys.stdout.flush()
                if doc_el.tag == 'doc':
                    doc_obj = doc_from_processed_kb_xml_elem(doc_el)
                    if passes_filters(doc_obj, filters):
                        enrich_doc_obj(doc_obj, enrichments)
                        # some extra metadata
                        doc_obj['zipfilename'] = zipfilename
                        # ingest document
                        if not bulk:
                            index_document(doc_obj, doc_obj['identifier'], INDEX_NAME, es)
                        else:
                            bulk_docs.append({
                                             "_index": INDEX_NAME,
                                             "_type": 'doc',
                                             "_id": doc_obj["identifier"],
                                             "_source": doc_obj.copy()
                                             })

                        article_count += 1
                    else:
                        filtered_count += 1
        if verbose: print("... docs filtered/ingested: " + str(filtered_count) + "/" + str(article_count))
        write_progress(logfile, article_count + filtered_count, article_count, filtered_count)
        if bulk: print(helpers.bulk(es, bulk_docs, chunk_size=10))
        if verbose: print("... done with file.")
        return (article_count)
    except Exception as error:
        print(type(error), "in", zipfilename + ":", error)
        import traceback
        traceback.print_exc()
        write_progress(logfile, article_count + filtered_count, article_count, filtered_count, success=False)
        return (0)


def ingest_docs_from_processed_kb_zip_file_FORKED(es_args, es_kwargs, *args, **kwargs):
    es = elasticsearch.ElasticSearch(*es_args, **es_kwargs)
    ingest_docs_from_processed_kb_zip_file(es, *args, **kwargs)


def create_index_texcavator(name, es):
    """
    Create a KB data index that is compatible with Texcavator. Other than that,
    a minimal amount of data from the KB dataset is used.
    Also includes a "raw" title field, which is not analyzed and thus searchable
    for full titles instead of just the separate words.
    """

    config = {}

    # See http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/analysis.html
    # An analyzer splits a field into tokens beforing indexing and searching.
    config['settings'] = {
    #    'number_of_shards': 3,
    #    'number_of_replicas': 1, 
        'analysis' : {
            'analyzer':{
                'default':{      # EGP: 'default' is a give name, not an ES default, I think
                    'type':'standard',
                    'stopwords': '_dutch_',
                }
            }
        }
    }

    # General info:
    # http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/mapping.html
    # About types, e.g. string, and options like "index not_analyzed":
    # http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/mapping-core-types.html
    # 20180314:
    # - string fields changed to keyword or text type (https://www.elastic.co/blog/strings-are-dead-long-live-strings)
    # - include_in_all is deprecated (actually, the _all field is deprecated); I don't think we used this extensively anyway, but it seems (at least for query string type queries) that the default behavior is still such that all fields are searched, so results should be the same as before
    # 
    # All fields are keyword or text, except for paper_dc_date, which is a date.
    metadata = {
             'type': 'keyword'
            }
    main_text = {
             'type': 'text', 
             'term_vector': 'with_positions_offsets_payloads'
             # This setting for term_vector stores statistics in the index.
             # See https://github.com/elasticsearch/elasticsearch/issues/3114
            }
    config['mappings'] = {'doc': {'properties': {
         'article_dc_subject': metadata,
         'article_dc_identifier': metadata,
         'identifier': metadata,
         'paper_dc_identifier': metadata,
         'filename': metadata,
         'zipfilename': metadata,
         'paper_dc_title_raw': metadata, # this one is useful in Kibana, you can then use a terms panel which gives all the newspaper names in full
         # dummy stuff (not actually in PM steekproef) for Texcavator
         'paper_dcterms_spatial': metadata,
         'paper_dcterms_temporal': metadata,

         'paper_dc_title': main_text,
         'text_content': main_text,
         'article_dc_title': main_text,  # name changed for Texcavator
         'paper_zuil': main_text,
         
         'paper_dc_date': {
             'format': 'dateOptionalTime', 
             'type': 'date'
         },

         # below stuff only used when loading full document:
         'article_dc_identifier_resolver': metadata,
         'article_dcterms_accessRights': metadata,
         'article_dcx_recordIdentifier': metadata,
         'paper_dc_identifier_resolver': metadata,
         'paper_dc_language': metadata,
         'paper_dc_publisher': metadata,
         'paper_dc_source': metadata,
         'paper_dcterms_alternative': metadata,
         'paper_dcterms_isPartOf': metadata,
         'paper_dcterms_isVersionOf': metadata,
         'paper_dcterms_issued': metadata,
         'paper_dcterms_spatial_creation': metadata,
         'paper_dcx_issuenumber': metadata,
         'paper_dcx_recordIdentifier': metadata,
         'paper_dcx_recordRights': metadata,
         'paper_dcx_volume': metadata,
         'paper_ddd_yearsDigitized': metadata,
    } } }
    es.indices.create(index=name, body=config, master_timeout='120s')

# DEFINE FILTERS
# - include: True/False; including filter if True, excluding filter if False
#   * exclude filters act like an eraser; they only act locally. As such they
#     can override inclusion of individual doc_objs, also if they are explicitly
#     included by an include filter.
#   * include filters act globally; when any is used everything that is not
#     included is automatically excluded
# - type: one of 'before', 'after', 'in', or 'equals'
#   * 'before'/'after':
#     + 'date' property; 'yyyy-mm-dd'
#   * 'in':
#     + 'field': field name to filter on
#     + 'list': list of strings which the field must be in
#   * 'equals':
#     + 'field': field name to filter on
#     + 'value': string that the field is compared to
interbellum = [
    {
        'include': True,
        'type': 'before',
        'date': '1940-01-01'
    },
    {
        'include': True,
        'type': 'after',
        'date': '1917-12-31'
    },
]
wederopbouw = [
    {
        'include': True,
        'type': 'before',
        'date': '1970-01-01'
    },
    {
        'include': True,
        'type': 'after',
        'date': '1945-12-31'
    },
]
modernetijd = [
    {
        'include': True,
        'type': 'before',
        'date': '2000-01-01'
    },
    {
        'include': True,
        'type': 'after',
        'date': '1969-12-31'
    },
]
twintigste_eeuw = [
    {
        'include': True,
        'type': 'before',
        'date': '2000-01-01'
    },
    {
        'include': True,
        'type': 'after',
        'date': '1899-12-31'
    },
]

twee_random_kranten = [
    {
        'include': True,
        'type': 'in',
        'field': 'paper_dc_title_raw',
        'list': [
                    'Algemeen Handelsblad',
                    'Drentsch dagblad : officieel orgaan voor de provincie Drenthe'
                ]
    }
]

katholieke_kranten_kb = [
    {
        'include': True,
        'type': 'in',
        'field': 'paper_dc_identifier',
        'list': [
                    "832688045",
                    "833066226",
                    "832369047",
                    "83245351X",
                    "832453390",
                    "842127143",
                    "842126635"
                ]
    }
]

katholieke_kranten_lokaal = [
    {
        'include': True,
        'type': 'in',
        'field': 'paper_dc_title_raw',
        'list': [
                    "Leidsche Courant",
                    "De Gelderlander"
                ]
    }
]

protestantse_kranten_kb = [
    {
        'include': True,
        'type': 'in',
        'field': 'paper_dc_identifier',
        'list': [
                    "832667536",
                    "810209039"
                ]
    }
]

socialistische_kranten_kb = [
    {
        'include': True,
        'type': 'in',
        'field': 'paper_dc_identifier',
        'list': [
                    "850279356",
                    "331833263",
                    "83308013X",
                    "841201854",
                    "832698679",
                    "850161290",
                    "832727143",
                    "850280184"
                ]
    }
]

liberale_kranten_kb = [
    {
        'include': True,
        'type': 'in',
        'field': 'paper_dc_identifier',
        'list': [
                    "852115210",
                    "82000541X",
                    "852121741",
                    "833050346",
                    "832337900",
                    "832495182",
                    "833050788",
                    "83249562X",
                    "832401439",
                    "832564818",
                    "83249531X",
                    "833013246",
                    "865061483",
                    "832675288",
                    "832689858"
                ]
    }
]

liberale_kranten_lokaal = [
    {
        'include': True,
        'type': 'in',
        'field': 'paper_dc_title_raw',
        'list': [
                    "Het Leidsch Dagblad",
                    "Haarlems Dagblad"
                ]
    }
]

# end filters

# enrichment objects
# zuilen
katholieke_zuil = {
    'type': 'add_field',
    'field_name': 'paper_zuil',
    'value': 'katholiek'
}
protestantse_zuil = {
    'type': 'add_field',
    'field_name': 'paper_zuil',
    'value': 'protestants'
}
socialistische_zuil = {
    'type': 'add_field',
    'field_name': 'paper_zuil',
    'value': 'socialistisch'
}
liberale_zuil = {
    'type': 'add_field',
    'field_name': 'paper_zuil',
    'value': 'liberaal'
}
niks_zuil = {
    'type': 'add_field',
    'field_name': 'paper_zuil',
    'value': 'niks'
}
# end enrichtment objects


if __name__  == '__main__':
    # --- INPUT PARAMETERS --- #
    parser = argparse.ArgumentParser(description='Set up a Texcavator compatible ElasticSearch instance of KB newspaper articles, enriched with pillar data from the PIDIMEHS project.')
    default_host, default_port = elasticsearch.Urllib3HttpConnection().host.split('/')[-1].split(':')
    parser.add_argument('input_dir', help="directory containing .data.gz input files (e.g. $SOMEWHERE/texcavator/processed/")
    parser.add_argument('server_address', nargs='?', default=default_host,
                        help='the address of your ElasticSearch server')
    parser.add_argument('-p', '--server_port', type=int, default=int(default_port))
    parser.add_argument('-n', '--no_auth', action='store_true',
                        help='use an unauthenticated connection (no username/password necessary)')
    parser.add_argument('--log_dir', default='kb_pidimehs_progress',
                        help='directory name where progress logs are stored')
    parser.add_argument('-t', '--testing', action='store_true',
                        help='testing flag: adds a timestamp to index name and progress-log directory')
    parser.add_argument('-j', '--jobs', type=int, default=int(1),
                        help='number of parallel jobs to run ingestion with')
    parser.add_argument('-P', '--process_pool', action='store_true',
                        help='use a process pool instead of a thread pool for parallel jobs')

    args = parser.parse_args()

    # --- DEFAULT PARAMETERS --- #
    input_dir = args.input_dir + "/"
    INDEX_NAME = 'kb_pidimehs'
    # use the minimal "Texcavator+-style" index format
    create_index = create_index_texcavator
    # choose filters
    # filters = interbellum + wederopbouw  # N.B.: DIT KAN NIET!
    ### include filters excluden alles wat er niet in zit, dus je kunt ze niet
    ### combineren!
    # filters = wederopbouw
    # filters = None
    # filters = twee_random_kranten
    # FILTEREN: alleen op periode, niet op zuilen, want de rest is achtergrond!
    filters = twintigste_eeuw # + \
              # katholieke_kranten_kb + protestantse_kranten_kb + \
              # socialistische_kranten_kb + liberale_kranten_kb
    # enrichment: filter plus enrichment object what to add to which field
    enrichments = [
        {
            'filter': katholieke_kranten_kb,
            'enrichment': katholieke_zuil
        },
        {
            'filter': katholieke_kranten_lokaal,
            'enrichment': katholieke_zuil
        },
        {
            'filter': protestantse_kranten_kb,
            'enrichment': protestantse_zuil
        },
        {
            'filter': socialistische_kranten_kb,
            'enrichment': socialistische_zuil
        },
        {
            'filter': liberale_kranten_kb,
            'enrichment': liberale_zuil
        },
        {
            'filter': liberale_kranten_lokaal,
            'enrichment':liberale_zuil
        },
        # {
        #     'filter': twee_random_kranten,
        #     'enrichment': niks_zuil
        # }
    ]
    # test flag
    testing = args.testing
    log_dir = args.log_dir
    # --- END PARAMETERS --- #

    # temporary stuff for testing
    if testing:
        now = datetime.datetime.now().isoformat().lower()
        INDEX_NAME += "_" + now
        log_dir = log_dir + now



    # get elasticsearch authentication information; use a lambda so that the
    # password isn't stored in this script itself (except perhaps in the
    # elasticsearch module)
    if args.no_auth:
        http_auth = lambda: None
    else:
        http_auth = lambda: (input("Enter ElasticSearch server user name: "),
                             getpass.getpass())

    # start elasticsearch connection
    es_args = (args.server_address,)
    es_kwargs = {'port': args.server_port,
                 'http_auth': http_auth(),
                 'maxsize': max(args.jobs, 10)}
    es = elasticsearch.Elasticsearch(*es_args, **es_kwargs)

    # wait a few seconds for elasticsearch to connect
    print("sleeping...")
    time.sleep(5)
    print("done sleeping")

    print("cluster health:")
    print(es.cluster.health())

    # create new index (if doesn't exist yet)
    print("create index")

    try:
        print("trying to create index...")
        create_index(INDEX_NAME, es)
    except elasticsearch.RequestError as error:
        print("creating index failed", error.error)
        if "resource_already_exists_exception" in error.error:
            flag_restart = input("Start over again (delete index and remove progress files) [y]\nor continue where you left off [N]?")

            # if flag_restart in ['', 'y', 'Y', 'yes', 'j', 'ja', 'oui', 'affirmative']:
            if flag_restart in ['yes', 'ja', 'oui', 'affirmative']:
                if INDEX_NAME == 'kb_texc-zuil_laptop':
                    raise SystemExit("\n\nYou probably don't want to start over, the database is already >100 GB!\nPlease do manually if you really want this.\n\n")
                es.indices.delete(INDEX_NAME)
                print("sleeping...")
                time.sleep(5)
                print("done sleeping")
                for filename in glob.glob(log_dir + '/*'):
                    os.remove(filename)
                create_index(INDEX_NAME, es)
        else:
            raise error

    # start ingesting files
    if testing:
        max_files = 3
    else:
        max_files = -1

    filelist = sorted(glob.glob(input_dir + "*.data.gz"))[:max_files]

    if args.jobs > 1:
        if not args.process_pool:
            # inspired on http://danshiebler.com/2016-09-14-parallel-progress-bar/
            with concurrent.futures.ThreadPoolExecutor(max_workers=args.jobs) as pool:
                futures = [pool.submit(ingest_docs_from_processed_kb_zip_file,
                                       es, filename, input_dir, INDEX_NAME,
                                       log_dir, filters=filters,
                                       enrichments=enrichments)
                           for filename in filelist]
                for _ in tqdm.tqdm(concurrent.futures.as_completed(futures),
                                   total=len(futures)):
                    pass
        else:
            del es  # kill the local instance to avoid errors with pickle
            with concurrent.futures.ProcessPoolExecutor(max_workers=args.jobs) as pool:
                futures = [pool.submit(ingest_docs_from_processed_kb_zip_file_FORKED,
                                       es_args, es_kwargs,
                                       filename, input_dir, INDEX_NAME,
                                       log_dir, filters=filters,
                                       enrichments=enrichments)
                           for filename in filelist]
                for _ in tqdm.tqdm(concurrent.futures.as_completed(futures),
                                   total=len(futures)):
                    pass
    else:
        for filename in tqdm.tqdm(filelist):
            ingest_docs_from_processed_kb_zip_file(es, filename, input_dir, INDEX_NAME, log_dir, filters=filters, enrichments=enrichments)
