import seaborn as sns
import copy

sns.set_style("darkgrid")

# Volgorde/lijnstijl grafieken:
cols = ("kat", "soc", "lib", "pro", "neutraal", "totaal")
cols_norm = ("kat_norm", "soc_norm", "lib_norm", "pro_norm", "neutraal_norm")
cols_kldiv = ("kat_kldiv", "soc_kldiv", "lib_kldiv", "pro_kldiv", "neutraal_kldiv")
linewidths = (1.5, 1.5, 1.5, 1.5, 1.5, 1)
linestyles = ("-", "-", "-", "-", "-.", "--")

# v2 (split liberals and neutrals, added "rest")
cols2 = ("kat", "soc", "lib", "pro", "neu", "rest", "totaal")
cols_norm2 = ("kat_norm", "soc_norm", "lib_norm", "pro_norm", "neu_norm", "rest_norm")
cols_kldiv2 = ("kat_kldiv", "soc_kldiv", "lib_kldiv", "pro_kldiv", "neu_kldiv", "rest_kldiv")
linewidths2 = (1.5, 1.5, 1.5, 1.5, 1.5, 1.5, 1)
linestyles2 = ("-", "-", "-", "-", "-", "-.", "--")

# v3 (split liberals and neutrals, removed "rest" again)
cols3 = ("kat", "soc", "lib", "pro", "neu", "totaal")
cols_norm3 = ("kat_norm", "soc_norm", "lib_norm", "pro_norm", "neu_norm")
cols_kldiv3 = ("kat_kldiv", "soc_kldiv", "lib_kldiv", "pro_kldiv", "neu_kldiv")
linewidths3 = (1.5, 1.5, 1.5, 1.5, 1.5, 1)
linestyles3 = ("-", "-", "-", "-", "-", "--")
"""
Kleur codering Gerrit & Huub:
* rood: soc
* geel: kat
* blauw: lib
* groen: arp (later cda)
* oranje: chu
* **zwart: totaal**
* *grijs & streepjeslijn: neutraal/zuilloos*

Probleem: de kranten zijn op zuil ingedeeld en de protestanten zijn daarbij samengenomen. Kunnen dus geen arp/chu splitsen. Bovendien hebben we tot op heden exact nul protestantse kranten.

Oplossing:

* paars: pro = groen (arp/cda) + oranje (chu)

Wijziging v2:
* groen: neu (na splitsing lib/neu), beter te onderscheiden dan cyaan en voor
         ARP hebben we toch geen lijntje nodig.
"""

# pal = sns.choose_colorbrewer_palette('qualitative')
pal = sns.color_palette()
paln = {
        "blue": pal[0],
        "green": pal[1],
        "red": pal[2],
        "purple": pal[3],
        "yellow": pal[4],
        "cyan": pal[5]
}
# op volgorde zetten:
pal = sns.color_palette((paln["yellow"], paln["red"], paln["blue"],
    paln["purple"], (0.5,0.5,0.5), (0.1,0.1,0.1)))
pal2 = sns.color_palette((paln["yellow"], paln["red"], paln["blue"],
    paln["purple"], paln["green"],
    (0.5,0.5,0.5), (0.1,0.1,0.1)))

# Handig alles bij elkaar in 1 tuple:
style = (cols, pal, linestyles, linewidths)
style_n = (cols_norm, pal, linestyles, linewidths)
style_kldiv = (cols_kldiv, pal, linestyles, linewidths)
style_dict = {
        'raw': style,
        'norm': style_n,
        'kldiv': style_kldiv
}
# v2
style2 = (cols2, pal2, linestyles2, linewidths2)
style_n2 = (cols_norm2, pal2, linestyles2, linewidths2)
style_kldiv2 = (cols_kldiv2, pal2, linestyles2, linewidths2)
style_dict2 = {
        'raw': style2,
        'norm': style_n2,
        'kldiv': style_kldiv2
}
# v3
style3 = (cols3, pal, linestyles3, linewidths3)
style_n3 = (cols_norm3, pal, linestyles3, linewidths3)
style_kldiv3 = (cols_kldiv3, pal, linestyles3, linewidths3)
style_dict3 = {
        'raw': style3,
        'norm': style_n3,
        'kldiv': style_kldiv3
}

# at some point, the palette seems to have changed... luckily we had it in raw
# RGB values in the kranten per zuil script:
colors_raw = [(0.8, 0.7254901960784313, 0.4549019607843137),
  (0.7686274509803922, 0.3058823529411765, 0.3215686274509804),
  (0.2980392156862745, 0.4470588235294118, 0.6901960784313725),
  (0.5058823529411764, 0.4470588235294118, 0.6980392156862745),
  (0.5, 0.5, 0.5),
  (0.1, 0.1, 0.1)]

pal_raw = sns.color_palette(colors_raw)

# cols3 = ("kat", "soc", "lib", "pro", "neu", "totaal")
linestyles4 = ("-", "-.", "--", ":", "--", "--")
# linewidths4 = (1.5, 1.5, 1.5, 1.5, 1.5, 1)
linewidths4 = (1, 1, 1, 1, 1, 2/3)

# v4
style4 = (cols3, pal_raw, linestyles4, linewidths4)
style_n4 = (cols_norm3, pal_raw, linestyles4, linewidths4)
style_kldiv4 = (cols_kldiv3, pal_raw, linestyles4, linewidths4)
style_dict4 = {
        'raw': style4,
        'norm': style_n4,
        'kldiv': style_kldiv4
}

# for without protestants
style_dict4_noPro = copy.deepcopy(style_dict4)
# convert to lists to be able to pop
for k, v in style_dict4_noPro.items():
    style_dict4_noPro[k] = tuple([list(item) for item in v])
for k, v in style_dict4_noPro.items():
    for ix in (0, 1, 3):
        v[ix].pop(3)
    v[2].pop(4)
# convert back to tuples
for k, v in style_dict4_noPro.items():
    style_dict4_noPro[k] = tuple([tuple(item) for item in v])

linewidths5 = (2/3,) * 5 + (0.5,)

# v5
style5 = (cols3, pal_raw, linestyles4, linewidths5)
style_n5 = (cols_norm3, pal_raw, linestyles4, linewidths5)
style_kldiv5 = (cols_kldiv3, pal_raw, linestyles4, linewidths5)
style_dict5 = {
        'raw': style5,
        'norm': style_n5,
        'kldiv': style_kldiv5
}

# for without protestants
style_dict5_noPro = copy.deepcopy(style_dict5)
# convert to lists to be able to pop
for k, v in style_dict5_noPro.items():
    style_dict5_noPro[k] = tuple([list(item) for item in v])
for k, v in style_dict5_noPro.items():
    for ix in (0, 1, 3):
        v[ix].pop(3)
    v[2].pop(4)
# convert back to tuples
for k, v in style_dict5_noPro.items():
    style_dict5_noPro[k] = tuple([tuple(item) for item in v])


# for without protestants AND liberals
style_dict4_noProLib = copy.deepcopy(style_dict4)
# convert to lists to be able to pop
for k, v in style_dict4_noProLib.items():
    style_dict4_noProLib[k] = tuple([list(item) for item in v])
for k, v in style_dict4_noProLib.items():
    for ix in (0, 1, 3):
        v[ix].pop(3)
        v[ix].pop(2)
    v[2].pop(4)
    v[2].pop(2)
# convert back to tuples
for k, v in style_dict4_noProLib.items():
    style_dict4_noProLib[k] = tuple([tuple(item) for item in v])


# finally, a subtle style for the axh/vlines, similar to gridline style
axline_style = {'color': (0, 0, 0), 'linewidth': 0.5, 'alpha': 0.5,
                'linestyle': ':'}


def show_palette(pal=pal):
    pal_o = sns.color_palette()
    print("Origineel seaborn palet:")
    sns.palplot(pal_o)
    print("Aangepast palet:")
    sns.palplot(pal)

