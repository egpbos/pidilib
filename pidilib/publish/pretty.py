#!/usr/bin/env python
from matplotlib import pyplot as plt
import matplotlib as mpl
import seaborn as sns

sns.reset_orig()
sns.set_style("ticks")

def pretty_axes(ax, fontsize=14, auto_gridlines=False):
    """
    Based on www.randalolson.com/2014/06/28/
    how-to-make-beautiful-data-visualizations-in-python-with-matplotlib/
    """
    # Remove the plot frame lines. They are unnecessary chartjunk.
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)

    # Turn off grid lines (Pandas adds them by default); or make them pretty
    if not auto_gridlines:
        ax.grid(False)
    else:
        pretty_ticklines(ax, redraw_existing=True)

    # Ensure that the axis ticks only show up on the bottom and left of the plot.
    # Ticks on the right and top of the plot are generally unnecessary chartjunk.
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()

    # Limit the range of the plot to only where the data is.
    # Avoid unnecessary whitespace.
    ax.axis('tight')

    # Make sure your axis ticks are large enough to be easily read.
    # You don't want your viewers squinting to read your plot.
    plt.yticks(fontsize=fontsize)
    plt.xticks(fontsize=fontsize)


def pretty_ticklines(ax, manual_y=[], manual_x=[], redraw_existing=False):
    """
    Based on www.randalolson.com/2014/06/28/
    how-to-make-beautiful-data-visualizations-in-python-with-matplotlib/
    """
    # Remove the remaining plot frame lines. They are unnecessary chartjunk.
    ax.spines["left"].set_visible(False)
    ax.spines["bottom"].set_visible(False)

    # Provide tick lines across the plot to help your viewers trace along
    # the axis ticks. Make sure that the lines are light and small so they
    # don't obscure the primary data lines.
    if redraw_existing:
        ax.xaxis.grid(linestyle="--", linewidth=0.5, color="black", alpha=0.3)
        ax.yaxis.grid(linestyle="--", linewidth=0.5, color="black", alpha=0.3)
#         for tl in ax.get_xticklines() + ax.get_yticklines():
#             tl.set_linestyle('dashed')
#             tl.set_linewidth(0.5)
#             tl.set_color('black')
#             tl.set_alpha(0.3)
    for y in manual_y:
        ax.plot(ax.get_xlim(), [y] * 2, "--", lw=0.5, color="black", alpha=0.3)
    for x in manual_x:
        ax.plot([x] * 2, ax.get_ylim(), "--", lw=0.5, color="black", alpha=0.3)

    # Remove the tick marks; they are unnecessary with the tick lines we just plotted.    
    ax.tick_params(axis="both", which="both", bottom="off", top="off",
                   labelbottom="on", left="off", right="off", labelleft="on")


# default_font = mpl.font_manager.FontProperties(size=14)
default_font = mpl.font_manager.FontProperties(size=12, family='serif')


def pretty_fonts(ax, default_props=default_font, title_props=None, xlabel_props=None,
                 ylabel_props=None, xtick_props=None, ytick_props=None, legend_props=None):
    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                  ax.get_xticklabels() + ax.get_yticklabels()
                  + ax.get_legend().get_texts()):
        item.set_fontproperties(default_props)
    if title_props is not None:
        ax.title.set_fontproperties(title_props)
    if xlabel_props is not None:
        ax.xaxis.label.set_fontproperties(xlabel_props)
    if ylabel_props is not None:
        ax.yaxis.label.set_fontproperties(ylabel_props)
    if xtick_props is not None:
        for item in ax.get_xticklabels():
            item.set_fontproperties(xtick_props)
    if ytick_props is not None:
        for item in ax.get_yticklabels():
            item.set_fontproperties(ytick_props)
    if legend_props is not None:
        for item in ax.get_legend().get_texts():
            item.set_fontproperties(legend_props)


def pretty(ax):
    pretty_axes(ax)
    pretty_fonts(ax)