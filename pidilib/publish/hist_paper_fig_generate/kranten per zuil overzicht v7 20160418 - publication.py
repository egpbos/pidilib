#!/usr/bin/env python

import sys
sys.path.append('../../..')

import matplotlib as mpl
mpl.use('pgf')
from matplotlib import pyplot as plt
import seaborn as sns
import numpy as np
import json
import pandas as pd
import datetime
import collections

import warnings

from copy import deepcopy

from elasticsearch import Elasticsearch
import certifi

import pidilib.elastic.queries as piq
import pidilib.bamboo as pib
import pidilib.publish.style as pistyle


zuilen_filters = piq.zuilen_kranten_filters_v5
style = pistyle.style_dict3

# ## Change to English labels

eng_label = {'kat': 'Catholic', 'soc': 'Socialist', 'lib': 'Liberal', 'pro': 'Protestant',
             'neu': 'neutral', 'total': 'total', 'totaal': 'total'}
NL_label = {'kat': 'Katholiek', 'soc': 'Socialistisch', 'lib': 'Liberaal', 'pro': 'Protestants',
             'neu': 'neutraal', 'total': 'totaal', 'totaal': 'totaal'}

style['raw'] = (('kat', 'soc', 'lib', 'pro', 'neu', 'total'),
 [(0.8, 0.7254901960784313, 0.4549019607843137),
  (0.7686274509803922, 0.3058823529411765, 0.3215686274509804),
  (0.2980392156862745, 0.4470588235294118, 0.6901960784313725),
  (0.5058823529411764, 0.4470588235294118, 0.6980392156862745),
  (0.5, 0.5, 0.5),
  (0.1, 0.1, 0.1)],
 ('-', '-', '-', '-', '-', '--'),
#  (1.5, 1.5, 1.5, 1.5, 1.5, 1))
 (2.5, 2.5, 2.5, 2.5, 2.5, 2))

style_old = deepcopy(style)

style_old['raw'] = (('kat', 'soc', 'lib', 'pro', 'neu', 'total'),
 [(0.8, 0.7254901960784313, 0.4549019607843137),
  (0.7686274509803922, 0.3058823529411765, 0.3215686274509804),
  (0.2980392156862745, 0.4470588235294118, 0.6901960784313725),
  (0.5058823529411764, 0.4470588235294118, 0.6980392156862745),
  (0.5, 0.5, 0.5),
  (0.1, 0.1, 0.1)],
 ('-', '-', '-', '-', '-', '--'),
 (1.5, 1.5, 1.5, 1.5, 1.5, 1))

ib_min = "1918-07-03"  # verkiezingen tweede kamer
ib_max = "1940-05-10"  # inval

wo_min = "1946-05-16"  # tweede kamer verkiezingen 1946
wo_max = "1967-02-15"  # tweede kamer verkiezingen 1967

SERVER = ['elastic']
PORT = 9200
INDEX_NAME = 'kb_pidimehs'


def generate_data():
    es = Elasticsearch(
        SERVER,
        port=PORT,
        timeout=600
    )

    index_kb = INDEX_NAME

    # Alle documenten per jaar per zuil

    filter_zuil = lambda zuil, t_min, t_max: piq.and_filter([zuilen_filters[zuil],
                                                             piq.date_range_filter(t_min, t_max)])
    opdracht_zuil = lambda zuil, t_min, t_max: {"size": 100000, #2**31-1,
                                                "query": piq.filtered_query(piq.all_query,
                                                                            filter_zuil(zuil, t_min, t_max)),
                                                "aggs": piq.arts_per_yr_agg}

    # interbellum
    doc_count_zuil = []

    for zuil_naam in zuilen_filters.keys():
        result_date_hist = es.search(index=index_kb, body=opdracht_zuil(zuil_naam, ib_min, ib_max))
        df = pib.df_from_arts_per_t_agg(result_date_hist, opdracht_zuil(zuil_naam, ib_min, ib_max),
                                        ib_min, ib_max, col_name=zuil_naam)
        doc_count_zuil.append(df)

    doc_count_ib = pd.concat(doc_count_zuil, axis=1)
    doc_count_ib["total"] = doc_count_ib.sum(axis=1)

    # wederopbouw
    doc_count_zuil = []

    for zuil_naam in zuilen_filters.keys():
        result_date_hist = es.search(index=index_kb, body=opdracht_zuil(zuil_naam, wo_min, wo_max))
        df = pib.df_from_arts_per_t_agg(result_date_hist, opdracht_zuil(zuil_naam, wo_min, wo_max),
                                        wo_min, wo_max, col_name=zuil_naam)
        doc_count_zuil.append(df)

    doc_count_wo = pd.concat(doc_count_zuil, axis=1)
    doc_count_wo["total"] = doc_count_wo.sum(axis=1)

    doc_count = doc_count_ib.append( doc_count_wo )

    doc_count = doc_count.reindex(pd.period_range(doc_count.index[0],
                                                  doc_count.index[-1],
                                                  freq='A')).fillna(0.0)

    # # Per paper per pillar

    opdracht_paper_pillar = lambda zuil, t_min, t_max: {"size": 100000, #2**31-1,
                                                        "query": piq.filtered_query(piq.all_query,
                                                                                    filter_zuil(zuil,
                                                                                                t_min,
                                                                                                t_max)),
                                                        "aggs": piq.paper_arts_per_yr_agg}

    doc_count_paper_pillar_dict = collections.defaultdict(list)

    for zuil_naam in zuilen_filters.keys():
        result_date_hist = es.search(index=index_kb, body=opdracht_paper_pillar(zuil_naam, ib_min, wo_max))
        for paper_bucket in result_date_hist['aggregations']['newspaper names']['buckets']:
            paper_name = paper_bucket['key']
            aggs_dict = {}
            aggs_dict['aggregations'] = paper_bucket
            df = pib.df_from_arts_per_t_agg(aggs_dict, opdracht_zuil(zuil_naam, ib_min, wo_max),
                                            ib_min, wo_max, col_name=paper_name)
            doc_count_paper_pillar_dict[zuil_naam].append(df)

    doc_count_paper_pillar = {}
    for zuil_name, paper_count_df_list in doc_count_paper_pillar_dict.items():
        doc_count_paper_pillar[zuil_name] = pd.concat(paper_count_df_list, axis=1)

    doc_count.to_pickle('df-articles_per_pillar.pickle')
    for key, value in doc_count_paper_pillar.items():
        value.to_pickle('df-articles_per_paper_%s.pickle' % key)


def load_data():
    doc_count = pd.read_pickle('df-articles_per_pillar.pickle')
    doc_count_paper_pillar = {}
    for zuil_name in zuilen_filters.keys():
        doc_count_paper_pillar[zuil_name] = pd.read_pickle('df-articles_per_paper_%s.pickle' % zuil_name)
    return doc_count, doc_count_paper_pillar


def center_year_bins_labels(ax, **kwargs):
    bins = ax.get_xticks()
    ax.set_xticks(bins + 0.5, **kwargs)
    ax.set_xticklabels(np.int32(bins))
    # ax.set_xlim(bins[0], bins[-1])


def stepstackplot(x, df, ax=None, **kwargs):
    """
    Must supply x manually. For instance, for a DataFrame df with a Period
    index, one can pass in df.index.year if the bins are yearly.
    The convention used here is that x must have the same number of entries
    as df has rows and that they indicate the start of the row bin interval.
    The final row bin edge is added by this function. It assumes regular bin
    intervals!
    Other than x, df and ax, this function takes the same kwargs as
    plt.stackplot.
    """
    if ax is None:
        fig, ax = plt.subplots(1, 1)
    
    # add extra step to the end to make a "full" stepped stackplot
    df_last_step = df.tail(1)
#     d_index = df.index[1] - df.index[0] # distance between index steps (assuming regular intervals)
#     df_last_step.index = df_last_step.index + d_index
    df_local = pd.concat([df, df_last_step])

    # add step to x as well
    d_x = x[1] - x[0]
    x_local = np.hstack((x, x[-1] + d_x))
    
    if kwargs.pop('step', False):
        warnings.warn("Step style is handled by this function, cannot be changed manually! "
                      "Removed given parameter from kwargs.")
    polygons = ax.stackplot(x_local, df_local.T, step='post', **kwargs)
    center_year_bins_labels(ax)

    patterns = (None, 'x', '*', '+', '\\', 'o', '.', 'O', '/', '-')
    for polygon, pattern in zip(polygons, patterns[:len(polygons)]):
        polygon.set_hatch(pattern)


#generate_data()
doc_count, doc_count_paper_pillar = load_data()


mpl.style.use(['seaborn-paper', 'seaborn-white', 'seaborn-deep',
               './pidimehs_paper_hi16.mplstyle', './line_plot.mplstyle'])

plt.rcParams.update({
    'pgf.rcfonts': False,
    'pgf.preamble': [
        r"\usepackage{fontspec}",
        r"\setmainfont{Lexicon No1 Roman A Med}",
        # r"\setmathfont{Lexicon No1 Roman A Med}",
    ],
    'font.family': 'Lexicon No1',
    "mathtext.default": "regular",
    "mathtext.it": "regular",
    "mathtext.rm": "regular",
    "mathtext.tt": "regular",
    "mathtext.bf": "regular",
    "mathtext.cal": "regular",
    "mathtext.sf": "regular",
})


# Alle documenten per jaar per zuil

fig, ax = plt.subplots(1, 1)

cols = list(style['raw'][0][:-1])
cols.remove('pro')
labels = [NL_label[col] for col in cols]
colors = style['raw'][1][:3] + style['raw'][1][4:]

stepstackplot(doc_count.index.year, doc_count[cols], ax=ax,
              colors=colors, labels=labels, baseline='wiggle')
ax.yaxis.set_major_formatter(mpl.ticker.FuncFormatter(lambda y, pos: (r'$%i$')%(abs(y*1e-5))))
ax.set_ylabel(r'artikelen (maal 100.000)')
ax.legend(loc='best', ncol=2)
ax.axis('tight')
ax.autoscale(axis='x', tight=True)
fig.tight_layout(pad=0.1)

plt.savefig('corpus_per_zuil.pdf')

with mpl.style.context(['./subfig49.mplstyle']):
    fig, ax = plt.subplots(1, 1)

    stepstackplot(doc_count.index.year, doc_count[cols], ax=ax,
                  colors=colors, labels=labels, baseline='wiggle')
    ax.yaxis.set_major_formatter(mpl.ticker.FuncFormatter(lambda y, pos: (r'$%i$')%(abs(y*1e-5))))
    ax.set_ylabel(r'artikelen (maal 100.000)')
    ax.legend(loc='best', ncol=2, handlelength=1, columnspacing=1, borderpad=-0.4,
              labelspacing=0.3)
    ax.axis('tight')
    ax.autoscale(axis='x', tight=True)
    fig.tight_layout(pad=0.1)
    # ax.text(0.5, 0.25, 'kat', transform=ax.transAxes)
    # pretty(ax)

plt.savefig('corpus_per_zuil_49.pdf')




# # Per paper per pillar

doc_count_paper_pillar_clean = deepcopy(doc_count_paper_pillar)
cols_clean = collections.defaultdict(list)

cols_clean['kat'].append('De Tijd: totaal')
cols_clean['kat'].extend([name for name in doc_count_paper_pillar_clean['kat'].columns.values
                          if name.lower()[:4] != 'de t'])
deTijd = [name for name in doc_count_paper_pillar_clean['kat'].columns.values
          if name.lower()[:4] == 'de t']
doc_count_paper_pillar_clean['kat']['De Tijd: totaal'] = doc_count_paper_pillar_clean['kat'][deTijd].sum(axis=1)

cols_clean['soc'].append('Het (vrije) Volk: totaal')
cols_clean['soc'].extend([name for name in doc_count_paper_pillar_clean['soc'].columns.values
                          if 'het volk' not in name.lower() and 'het vr' not in name.lower()])
hetVolk = [name for name in doc_count_paper_pillar_clean['soc'].columns.values
           if 'het volk' in name.lower() or 'het vr' in name.lower()]
doc_count_paper_pillar_clean['soc']['Het (vrije) Volk: totaal'] = doc_count_paper_pillar_clean['soc'][hetVolk].sum(axis=1)

cols_clean['neu'].append(u'Leeuwarder Courant: totaal')
cols_clean['neu'].extend([name for name in doc_count_paper_pillar_clean['neu'].columns.values
                          if name.lower()[:10] != 'leeuwarder'])
leeuwarder = [name for name in doc_count_paper_pillar_clean['neu'].columns.values
              if name.lower()[:10] == 'leeuwarder']
doc_count_paper_pillar_clean['neu'][u'Leeuwarder Courant: totaal'] = doc_count_paper_pillar_clean['neu'][leeuwarder].sum(axis=1)

cols_clean['lib'] = doc_count_paper_pillar_clean['lib'].columns.values
cols_clean['pro'] = doc_count_paper_pillar_clean['pro'].columns.values


colors_shades = {}
colors_husl = {}
for ix, color in enumerate(style['raw'][1][:-1]):
    zuil_name = style['raw'][0][ix]
    n_papers = len(cols_clean[zuil_name])
    colors_shades[zuil_name] = list(sns.light_palette(color, n_colors=n_papers+1)[1:])
    colors_husl[zuil_name]   = list(sns.husl_palette(n_colors=n_papers, s=0.3))

colors_per_zuil = {'pro': colors_shades['pro'],
                   'neu': colors_husl['neu'],
                   'kat': colors_shades['kat'],
                   'soc': colors_shades['soc'],
                   'lib': colors_shades['lib'],}

labels_acronyms = {}

# labels_acronyms['pro'] = [u'De Standaard']
# labels_acronyms['neu'] = [u'Leeuwarder Courant', u'De Telegraaf', u'Nieuwsblad van het Noorden', u'De Gooi- en Eemlander ', u'Nieuwe Tilburgsche Courant', u'Rotterdamsch nieuwsblad', u'Over\u0133sselsche en Zwolsche courant ', u'Middelburgsche courant', u'Drentsche en Asser courant']
# labels_acronyms['soc'] = ['Het (vrije) Volk', u'Voorwaarts ', u'Utrechts volksblad ']
# labels_acronyms['lib'] = [u'Algemeen Handelsblad', u'Het Vaderland ', u'Nieuwe Rotterdamsche Courant', u'Arnhemsche courant']
# labels_acronyms['kat'] = ['De Tijd', u'Limburgsch dagblad', u'Limburger koerier ', u'Het Centrum', u'Tilburgsche courant']
labels_acronyms['pro'] = [u'dS']
labels_acronyms['neu'] = [u'LC', u'dTG', u'NvhN', u'dGE', u'NTC', u'RN', u'OZC', u'MC', u'DAC']
labels_acronyms['soc'] = ['hVV', u'VW', u'UV']
labels_acronyms['lib'] = [u'AH', u'hV', u'NRC', u'AC']
labels_acronyms['kat'] = ['Tijd', u'LD', u'LK ', u'hC', u'TC']

use_acronyms = True

legend_settings = {}
legend_settings['soc'] = {'loc': 'best', 'handlelength': 1, 'ncol': 2,
                          'columnspacing': 1, 'borderpad': 0}
legend_settings['kat'] = {'loc': 'upper right', 'handlelength': 1, 'ncol': 2,
                          'columnspacing': 1, 'borderpad': 0}
legend_settings['lib'] = {'loc': 'best', 'handlelength': 1}
legend_settings['pro'] = {'loc': 'best', 'handlelength': 1}
neu_pad = 0.3
legend_settings['neu'] = {'loc': 'upper right', 'bbox_to_anchor': (0.96, 1),
                          'handlelength': 1, 'ncol': 2,
                          'columnspacing': neu_pad, 'borderaxespad': 0,
                          'handletextpad': neu_pad, 'labelspacing': 0.1,
                          'borderpad': 0}

with mpl.style.context(['./subfig49.mplstyle']):

    for ix, (zuil_name, count) in enumerate(doc_count_paper_pillar_clean.items()):
        fig, ax = plt.subplots(1, 1)

        cols = cols_clean[zuil_name]
        if use_acronyms:
            labels = labels_acronyms[zuil_name]
        else:
            labels = []
            for col in cols:
                label = col
                if ':' in label:
                    label = label[:label.find(':')]
                if "Provinciale " in label:
                    label = label[12:]
                labels.append(label)

        stepstackplot(count.index.year, count[cols], ax=ax, labels=labels,
                      colors=colors_per_zuil[zuil_name])

        ax.legend(**legend_settings[zuil_name])
        # ax.set_title(eng_label[zuil_name])

        ax.yaxis.set_major_formatter(mpl.ticker.FuncFormatter(lambda y, pos: (r'$%i$')%(abs(y*1e-4))))
        ax.set_ylabel(r'artikelen ($\times 10000$)')
        # ax.legend(loc='best', ncol=2, handlelength=1, columnspacing=1, borderpad=-0.4,
        #           labelspacing=0.3)
        ax.axis('tight')
        ax.autoscale(axis='x', tight=True)
        fig.tight_layout(pad=0.1)

        plt.savefig("corpus_%s.pdf" % zuil_name)


