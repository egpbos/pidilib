# Sources of inspiration:
# http://scipy.github.io/old-wiki/pages/Cookbook/Matplotlib/LaTeX_Examples
# http://matplotlib.org/users/style_sheets.html#style-sheets
# http://nipunbatra.github.io/2014/08/latexify/
# http://bkanuka.com/articles/native-latex-plots/
# https://www.bastibl.net/publication-quality-plots/

# This style sheet is based on the Seaborn styles, so load Seaborn first to
# be sure to get the same result. For the thesis we'll use:
# - `sns.set(context='paper', style='darkgrid')` for slab_plot
#
# The sheet was tested with darkgrid/notebook as well, which gives slightly
# larger padding between tick labels and axis and also larger points.
#
# Alternatively, one can do without Seaborn by using mpl.style.use or .context
# functions with the Matplotlib clones of the Seaborn styles, like so:
# ['seaborn-paper', 'seaborn-darkgrid', 'seaborn-deep', './thesis.mplstyle']
# However, this omits setting patch.facecolor! To fix that, also load the
# fix_seaborn-deep.mplstyle sheet, or wait for my issue to be fixed
# (https://github.com/matplotlib/matplotlib/issues/6307).


### FIGURE SIZE
# Found by putting \the\textwidth in the tex file, which is replaced by the
# textwidth number in pt in the compiled pdf. Then run in Python
# (using the golden ratio to define the default height based on the width):
# import numpy as np; textwidth_pt = 412.56496; inches_per_pt = 1.0/72.27; textwidth_in = textwidth_pt * inches_per_pt; golden_ratio = (np.sqrt(5.0)-1.0)/2.0; height_in = golden_ratio * textwidth_in; print textwidth_in, ", ", height_in

figure.figsize : 5.70866140861, 3.52814678078


### FONT
# From https://www.bastibl.net/publication-quality-plots/
#text.usetex: True
#font.family: serif
#font.serif: computer modern roman


### FONT SIZES
font.size: 10
axes.labelsize: 10  # default for everything, also colorbar
xtick.labelsize: 10
ytick.labelsize: 10
legend.fontsize: 10


### PGF STUFF
#pgf.texsystem: pdflatex  # THIS GIVES PROBLEMS WITH UNICODE! 
#pgf.preamble: \usepackage[utf8x]{inputenc}, \usepackage[T1]{fontenc}
pgf.texsystem: lualatex
# The unicode problem was caused by utf8x in combination with pdflatex!
#pgf.preamble: \usepackage[utf8]{inputenc}, \usepackage[T1]{fontenc}, \usepackage{mathptmx}
# 21 may 2020: inputenc and fontenc are no longer available in texlive2020, replaced by latex-uni8 (and really inputenc is not even necessary anymore since 2018, but ok); also mathptmx is obsolete and can be replaced by newtx
#pgf.preamble: \usepackage[T1]{fontenc}, \usepackage{newtxmath}

### IMAGES
image.interpolation: nearest
# Dit zorgt ervoor dat een grid gewoon geplot wordt zoals je het verwacht
# ((0,0) komt links onderin):
image.origin: lower
# Betere default colormap
# (https://github.com/matplotlib/matplotlib/issues/875)
image.cmap: hot


### AXES
# Following http://www.randalolson.com/2014/06/28/how-to-make-beautiful-data-visualizations-in-python-with-matplotlib/
# we remove the all axes spines by default. This is necessary to also remove
# them from colorbars. They can be manually reactivated for regular plots.
axes.spines.bottom: False
axes.spines.left: False
axes.spines.right: False
axes.spines.top: False
