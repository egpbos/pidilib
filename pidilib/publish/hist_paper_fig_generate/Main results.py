#!/usr/bin/env python

# coding: utf-8

# The results are grouped by four different indicators of pillarization:
# 
# * Party names
# * Party front runners (in election time)
# * Organizations (non-political, but pillarized)
# * Ideological terms
# 
# In this document, we only show the clearest and most interesting frequency or (specific) Kullback-Leibler plots from a historical perspective. In our full analysis, we also considered a fifth indicator, *Members of cabinet (government) and parliament (also opposition)*. The frequencies of this indicator were too low and probably dominated by noise, so we omit it here.
# 
# We focus on the periods of "interbellum" and "wederopbouw" (rebuilding), specifically from the elections of 1918-07-03 until the elections of 1967-02-15. This period saw the peak of pillarization before WOII and the beginning of depillarization at the end of the 60's.
# 
# In general, the strongest signals were found for the socialist indicators, followed by the catholic indicators. The liberal indicators are quite weak, which reflects their low degree of cohesion (whether they should be called a pillar at all is a matter of debate). For the protestants we lacked the most important newspaper over the period, so we excluded them from most of our analysis.

# # iPython notebook initialization
# N.B.: normalization needs an internet connection to the SURFsara Elasticsearch instance!

# no longer necessary in Python 3:
# import sys
# reload(sys)
# sys.setdefaultencoding('utf8')

# get_ipython().magic(u'matplotlib inline')

import sys
sys.path.append('../../..')

import matplotlib as mpl
mpl.use('pgf')
from matplotlib import pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
from elasticsearch import Elasticsearch
import certifi
import dateutil

import pidilib.elastic.queries as piq
import pidilib.bamboo as pib
import pidilib.data.partijen
import pidilib.publish.style as pistyle
# import pidilib.publish.pretty as pipretty
# prep = pipretty.pretty
import pidilib.data.personen
import pidilib.data.organisaties as org
import pidilib.data.ideologische_termen

import os


# CONFIGURATION
period_min = "1918-07-03"  # elections parliament
ib_max = "1940-05-10"  # invasion
# period_min = "1946-07-03"  # start cabinet Beel I
wo_min = "1946-05-16"  # elections parliament
period_max = "1967-02-15"  # elections parliament

SERVER = ['elastic']
PORT = 9200
INDEX_NAME = 'kb_pidimehs'

reraise = True

# sns.set_context('talk', font_scale=1.2)
# sns.set_style('darkgrid')

zuilen_filters = piq.zuilen_kranten_filters_v5
style = pistyle.style_dict4_noPro

es = Elasticsearch(
    SERVER,
    port=PORT,
    timeout=600
)

index_kb = INDEX_NAME

eng_label = {'kat': 'Catholic', 'soc': 'Socialist', 'lib': 'Liberal', 'pro': 'Protestant',
             'neu': 'neutral', 'total': 'total', 'totaal': 'total'}
NL_label = {'kat': 'Katholiek', 'soc': 'Socialistisch', 'lib': 'Liberaal', 'pro': 'Protestants',
             'neu': 'neutraal', 'total': 'totaal', 'totaal': 'totaal'}

cols = list(style['raw'][0][:-1])
labels = [NL_label[col] for col in cols]


def generate_normalization_data():
    # All documents per year per pillar
    filter_zuil = lambda zuil: piq.and_filter([zuilen_filters[zuil],
                                               piq.date_range_filter(period_min, period_max)])
    opdracht_zuil = lambda zuil: {"size": 0,
                                  "query": piq.filtered_query(piq.all_query, filter_zuil(zuil)),
                                  "aggs": piq.arts_per_yr_agg}

    doc_count_zuil = []

    for zuil_naam in zuilen_filters.keys():
        result_date_hist = es.search(index=index_kb, body=opdracht_zuil(zuil_naam))
        df = pib.df_from_arts_per_t_agg(result_date_hist, opdracht_zuil(zuil_naam), period_min, period_max,
                                        col_name=zuil_naam)
        doc_count_zuil.append(df)

    doc_count = pd.concat(doc_count_zuil, axis=1)
    doc_count["totaal"] = doc_count.sum(axis=1)

    # ax = pib.ppzuil(doc_count, style['raw'], size_str='small')

    # Interbellum
    filter_zuil = lambda zuil: piq.and_filter([zuilen_filters[zuil],
                                               piq.date_range_filter(period_min, ib_max)])
    opdracht_zuil = lambda zuil: {"size": 0,
                                  "query": piq.filtered_query(piq.all_query, filter_zuil(zuil)),
                                  "aggs": piq.arts_per_yr_agg}

    doc_count_zuil_ib = []

    for zuil_naam in zuilen_filters.keys():
        result_date_hist = es.search(index=index_kb, body=opdracht_zuil(zuil_naam))
        df = pib.df_from_arts_per_t_agg(result_date_hist, opdracht_zuil(zuil_naam), period_min, ib_max,
                                        col_name=zuil_naam)
        doc_count_zuil_ib.append(df)

    doc_count_ib = pd.concat(doc_count_zuil_ib, axis=1)
    doc_count_ib["totaal"] = doc_count_ib.sum(axis=1)

    # ax = pib.ppzuil(doc_count_ib, style['raw'], size_str='small')

    # Wederopbouw
    filter_zuil = lambda zuil: piq.and_filter([zuilen_filters[zuil],
                                               piq.date_range_filter(wo_min, period_max)])
    opdracht_zuil = lambda zuil: {"size": 0,
                                  "query": piq.filtered_query(piq.all_query, filter_zuil(zuil)),
                                  "aggs": piq.arts_per_yr_agg}

    doc_count_zuil_wo = []

    for zuil_naam in zuilen_filters.keys():
        result_date_hist = es.search(index=index_kb, body=opdracht_zuil(zuil_naam))
        df = pib.df_from_arts_per_t_agg(result_date_hist, opdracht_zuil(zuil_naam), wo_min, period_max,
                                        col_name=zuil_naam)
        doc_count_zuil_wo.append(df)

    doc_count_wo = pd.concat(doc_count_zuil_wo, axis=1)
    doc_count_wo["totaal"] = doc_count_wo.sum(axis=1)

    # ax = pib.ppzuil(doc_count_wo, style['raw'], size_str='small')


    # MONTHLY NORMALIZATION

    # All documents per month per pillar
    filter_zuil = lambda zuil: piq.and_filter([zuilen_filters[zuil],
                                               piq.date_range_filter(period_min, period_max)])
    opdracht_zuil_mn = lambda zuil: {"size": 0,
                                    "query": piq.filtered_query(piq.all_query, filter_zuil(zuil)),
                                    "aggs": piq.arts_per_mn_agg}

    doc_count_zuil_mn = []

    for zuil_naam in zuilen_filters.keys():
        result_date_hist = es.search(index=index_kb, body=opdracht_zuil_mn(zuil_naam))
        df = pib.df_from_arts_per_t_agg(result_date_hist, opdracht_zuil_mn(zuil_naam), period_min, period_max,
                                        col_name=zuil_naam)
        doc_count_zuil_mn.append(df)

    doc_count_mn = pd.concat(doc_count_zuil_mn, axis=1)
    doc_count_mn["totaal"] = doc_count_mn.sum(axis=1)

    # pib.ppzuil(doc_count_mn, style['raw'], size_str='medium')

    # plt.show()

    try:
        os.mkdir('main_results_data')
    except OSError:
        print("data directory already exists")

    doc_count.to_pickle('main_results_data/df-articles_total.pickle')
    doc_count_ib.to_pickle('main_results_data/df-articles_total_ib.pickle')
    doc_count_wo.to_pickle('main_results_data/df-articles_total_wo.pickle')
    doc_count_mn.to_pickle('main_results_data/df-articles_total_month.pickle')


def load_normalization_data():
    doc_count = pd.read_pickle('main_results_data/df-articles_total.pickle')
    doc_count_ib = pd.read_pickle('main_results_data/df-articles_total_ib.pickle')
    doc_count_wo = pd.read_pickle('main_results_data/df-articles_total_wo.pickle')
    doc_count_mn = pd.read_pickle('main_results_data/df-articles_total_month.pickle')

    return doc_count, doc_count_ib, doc_count_wo, doc_count_mn


# generate_normalization_data()
doc_count, doc_count_ib, doc_count_wo, doc_count_mn = load_normalization_data()


mpl.style.use(['seaborn-paper', 'seaborn-white', 'seaborn-deep',
               './pidimehs_paper_hi16.mplstyle', './line_plot.mplstyle'])

plt.rcParams.update({
    'pgf.rcfonts': False,
    'pgf.preamble': [
        r"\usepackage{fontspec}",
        r"\setmainfont{Lexicon No1 Roman A Med}",
    ],
    'font.family': 'Lexicon No1',
    # "mathtext.default": "regular",
    # "mathtext.it": "regular",
    # "mathtext.rm": "regular",
    # "mathtext.tt": "regular",
    # "mathtext.bf": "regular",
    # "mathtext.cal": "regular",
    # "mathtext.sf": "regular",
})


# # Party names

soc_parties = pidilib.data.partijen.sdap_v5 + "\n" + pidilib.data.partijen.pvda_v5

kat_parties = pidilib.data.partijen.rksp_v5 + "\n" + pidilib.data.partijen.abrkv_v5 +              "\n" + pidilib.data.partijen.kvp_v5

with mpl.style.context(['./subfig49.mplstyle']):
    fig, ax = plt.subplots(1, 1)

    pib.plot_kldiv_search_per_zuil(soc_parties, period_min, ib_max, doc_count,
                                   es, index_kb, zuilen_filters=zuilen_filters, style=style,
                                   # title="Socialist party names (SDAP \& PvdA)",
                                   title="", labels=labels, ax=ax,
                                   figsize=None, xlabel="jaar", ylabel="", add_legend=False,
                                   apply_period=True,
                                   cache_df_fn="main_results_data/party_names_soc.pickle",
                                   output_table_filename="party_names_soc_ib.html")

    pib.plot_kldiv_search_per_zuil(soc_parties, wo_min, period_max, doc_count,
                                   es, index_kb, zuilen_filters=zuilen_filters, style=pistyle.style_dict4_noProLib,
                                   # title="Socialist party names (SDAP \& PvdA)",
                                   title="", labels=labels, ax=ax,
                                   figsize=None, xlabel="jaar", ylabel="", add_legend=False,
                                   apply_period=True,
                                   cache_df_fn="main_results_data/party_names_soc.pickle",
                                   output_table_filename="party_names_soc_wo.html")

    ymin, ymax = plt.ylim()

    try:
        plt.tight_layout(pad=0.1)
        plt.savefig("party_names_soc.pdf")
    except Exception as e:
        print("RuntimeError while trying to layout and/or save plot, probably pdflatex could not be found or it couldn't find a font.")
        if reraise:
            raise e

    # fig, ax = plt.subplots(1, 1)

    # pib.plot_kldiv_search_per_zuil(soc_parties, period_min, ib_max, doc_count_mn,
    #                                es, index_kb, zuilen_filters=zuilen_filters, style=pistyle.style_dict5,
    #                                # title="Socialist party names (SDAP \& PvdA)",
    #                                title="",
    #                                agg=piq.arts_per_mn_agg,
    #                                apply_period=True, ax=ax,
    #                                figsize=None, xlabel="maand", ylabel="", add_legend=False,
    #                                cache_df_fn="main_results_data/party_names_month_soc.pickle")

    # pib.plot_kldiv_search_per_zuil(soc_parties, wo_min, period_max, doc_count_mn,
    #                                es, index_kb, zuilen_filters=zuilen_filters, style=pistyle.style_dict5,
    #                                # title="Socialist party names (SDAP \& PvdA)",
    #                                title="",
    #                                agg=piq.arts_per_mn_agg,
    #                                apply_period=True, ax=ax,
    #                                figsize=None, xlabel="maand", ylabel="", add_legend=False,
    #                                cache_df_fn="main_results_data/party_names_month_soc.pickle")

    # plt.tight_layout(pad=0.1)
    # plt.savefig("party_names_month_soc.pdf")

    fig, ax = plt.subplots(1, 1)

    pib.plot_kldiv_search_per_zuil(kat_parties, period_min, ib_max, doc_count,
                                   es, index_kb, zuilen_filters=zuilen_filters, style=style,
                                   # title="Catholic party names (RKSP, ABRKV \& KVP)",
                                   title="",
                                   apply_period=True, ax=ax,
                                   figsize=None, xlabel="jaar", ylabel="", add_legend=False,
                                   cache_df_fn="main_results_data/party_names_kat.pickle",
                                   output_table_filename="party_names_kat_ib.html")

    pib.plot_kldiv_search_per_zuil(kat_parties, wo_min, period_max, doc_count,
                                   es, index_kb, zuilen_filters=zuilen_filters, style=pistyle.style_dict4_noProLib,
                                   # title="Catholic party names (RKSP, ABRKV \& KVP)",
                                   title="",
                                   apply_period=True, ax=ax,
                                   figsize=None, xlabel="jaar", ylabel="", add_legend=False,
                                   cache_df_fn="main_results_data/party_names_kat.pickle",
                                   output_table_filename="party_names_kat_wo.html")

    plt.ylim((ymin, ymax))

    try:
        plt.tight_layout(pad=0.1)
        plt.savefig("party_names_kat.pdf")
    except Exception as e:
        print("RuntimeError while trying to layout and/or save plot, probably pdflatex could not be found or it couldn't find a font.")
        if reraise:
            raise e


# # Party front runners (election time)


lt_df = pidilib.data.personen.lijsttrekkers_df

soc_front = (lt_df.partij == 'SDAP') | (lt_df.partij == 'PvdA')
kat_front = (lt_df.partij == "RKSP") | (lt_df.partij == "RK") | (lt_df.partij == "KVP")

lt_df[soc_front | kat_front].to_html("lijsttrekkers_ib.html")

soc_front_qsq_ib = piq.phrases_str_from_persons_simple(lt_df[soc_front])
kat_front_qsq_ib = piq.phrases_str_from_persons_simple(lt_df[kat_front])

# post war; N.B.: this was missing before 2018!
# N.B. 2: Again edited in december 2018 to switch to corrected postwar front runners list, see 2018 front runners handling below (before Drees and other specific front runners)

ltpw_df = pidilib.data.personen.lijsttrekkers_postwar_df

soc_front_wo = ((ltpw_df.partij == 'SDAP') | (ltpw_df.partij == 'PvdA')) & (ltpw_df.begin_jaar < 1968)
kat_front_wo = ((ltpw_df.partij == "RKSP") | (ltpw_df.partij == "RK") | (ltpw_df.partij == "KVP")) & (ltpw_df.begin_jaar < 1968)

soc_front_qsq_wo = piq.phrases_str_from_persons_simple(ltpw_df[soc_front_wo])
kat_front_qsq_wo = piq.phrases_str_from_persons_simple(ltpw_df[kat_front_wo])

# together:
soc_front_qsq = soc_front_qsq_ib + "\n" + soc_front_qsq_wo
kat_front_qsq = kat_front_qsq_ib + "\n" + kat_front_qsq_wo

with mpl.style.context(['./subfig49.mplstyle']):
    fig, ax = plt.subplots(1, 1)

    pib.plot_kldiv_search_per_zuil(soc_front_qsq, period_min, ib_max, doc_count,
                                   es, index_kb, zuilen_filters=zuilen_filters, style=style,
                                   # title="Socialist party front runners (SDAP \& PvdA)",
                                   title="",
                                   apply_period=True, ax=ax,
                                   figsize=None, xlabel="jaar", ylabel="", add_legend=False,
                                   cache_df_fn="main_results_data/front_runners_soc+wo.pickle",
                                   output_table_filename="front_runners_soc+wo_ib.html")

    pib.plot_kldiv_search_per_zuil(soc_front_qsq, wo_min, period_max, doc_count,
                                   es, index_kb, zuilen_filters=zuilen_filters, style=pistyle.style_dict4_noProLib,
                                   # title="Socialist party front runners (SDAP \& PvdA)",
                                   title="",
                                   apply_period=True, ax=ax,
                                   figsize=None, xlabel="jaar", ylabel="", add_legend=False,
                                   cache_df_fn="main_results_data/front_runners_soc+wo.pickle",
                                   output_table_filename="front_runners_soc+wo_wo.html")

    ymin, ymax = plt.ylim()

    try:
        plt.tight_layout(pad=0.1)
        plt.savefig("front_runners_soc+wo.pdf")
    except Exception as e:
        print("RuntimeError while trying to layout and/or save plot, probably pdflatex could not be found or it couldn't find a font.")
        if reraise:
            raise e

    fig, ax = plt.subplots(1, 1)

    pib.plot_kldiv_search_per_zuil(kat_front_qsq, period_min, ib_max, doc_count,
                                   es, index_kb, zuilen_filters=zuilen_filters, style=style,
                                   # title="Catholic party front runners (RKSP, ABRKV \& KVP)",
                                   title="",
                                   apply_period=True, ax=ax,
                                   figsize=None, xlabel="jaar", ylabel="", add_legend=False,
                                   cache_df_fn="main_results_data/front_runners_kat+wo.pickle",
                                   output_table_filename="front_runners_kat+wo_ib.html")

    pib.plot_kldiv_search_per_zuil(kat_front_qsq, wo_min, period_max, doc_count,
                                   es, index_kb, zuilen_filters=zuilen_filters, style=pistyle.style_dict4_noProLib,
                                   # title="Catholic party front runners (RKSP, ABRKV \& KVP)",
                                   title="",
                                   apply_period=True, ax=ax,
                                   figsize=None, xlabel="jaar", ylabel="", add_legend=False,
                                   cache_df_fn="main_results_data/front_runners_kat+wo.pickle",
                                   output_table_filename="front_runners_kat+wo_wo.html")


    plt.ylim((ymin, ymax))

    try:
        plt.tight_layout(pad=0.1)
        plt.savefig("front_runners_kat+wo.pdf")
    except Exception as e:
        print("RuntimeError while trying to layout and/or save plot, probably pdflatex could not be found or it couldn't find a font.")
        if reraise:
            raise e


# december 2018 corrected postwar front runners
ltpw18_df = pidilib.data.personen.lijsttrekkers_2018_postwar_df

soc_front_wo18 = ((ltpw18_df.partij == 'SDAP') | (ltpw18_df.partij == 'PvdA')) & (ltpw18_df.begin_jaar < 1968)
kat_front_wo18 = ((ltpw18_df.partij == "RKSP") | (ltpw18_df.partij == "RK") | (ltpw18_df.partij == "KVP")) & (ltpw18_df.begin_jaar < 1968)

ltpw18_df[soc_front_wo18 | kat_front_wo18].to_html("lijsttrekkers_wo.html")

soc_front_qsq_wo18 = piq.phrases_str_from_persons_simple(ltpw18_df[soc_front_wo18])
kat_front_qsq_wo18 = piq.phrases_str_from_persons_simple(ltpw18_df[kat_front_wo18])

with mpl.style.context(['./subfig49.mplstyle']):
    fig, ax = plt.subplots(1, 1)

    pib.plot_kldiv_search_per_zuil(soc_front_qsq_ib, period_min, ib_max, doc_count,
                                   es, index_kb, zuilen_filters=zuilen_filters, style=style,
                                   # title="Socialist party front runners (SDAP \& PvdA)",
                                   title="",
                                   apply_period=True, ax=ax,
                                   figsize=None, xlabel="jaar", ylabel="", add_legend=False,
                                   cache_df_fn="main_results_data/front_runners_soc+wo_2018_ib.pickle",
                                   output_table_filename="front_runners_soc+wo_2018_ib.html")

    pib.plot_kldiv_search_per_zuil(soc_front_qsq_wo18, wo_min, period_max, doc_count,
                                   es, index_kb, zuilen_filters=zuilen_filters, style=pistyle.style_dict4_noProLib,
                                   # title="Socialist party front runners (SDAP \& PvdA)",
                                   title="",
                                   apply_period=True, ax=ax,
                                   figsize=None, xlabel="jaar", ylabel="", add_legend=False,
                                   cache_df_fn="main_results_data/front_runners_soc+wo_2018_wo.pickle",
                                   output_table_filename="front_runners_soc+wo_2018_wo.html")

    ymin, ymax = plt.ylim()

    try:
        plt.tight_layout(pad=0.1)
        plt.savefig("front_runners_soc+wo_2018.pdf")
    except Exception as e:
        print("RuntimeError while trying to layout and/or save plot, probably pdflatex could not be found or it couldn't find a font.")
        if reraise:
            raise e

    fig, ax = plt.subplots(1, 1)

    pib.plot_kldiv_search_per_zuil(kat_front_qsq_ib, period_min, ib_max, doc_count,
                                   es, index_kb, zuilen_filters=zuilen_filters, style=style,
                                   # title="Catholic party front runners (RKSP, ABRKV \& KVP)",
                                   title="",
                                   apply_period=True, ax=ax,
                                   figsize=None, xlabel="jaar", ylabel="", add_legend=False,
                                   cache_df_fn="main_results_data/front_runners_kat+wo_2018_ib.pickle",
                                   output_table_filename="front_runners_kat+wo_2018_ib.html")

    pib.plot_kldiv_search_per_zuil(kat_front_qsq_wo18, wo_min, period_max, doc_count,
                                   es, index_kb, zuilen_filters=zuilen_filters, style=pistyle.style_dict4_noProLib,
                                   # title="Catholic party front runners (RKSP, ABRKV \& KVP)",
                                   title="",
                                   apply_period=True, ax=ax,
                                   figsize=None, xlabel="jaar", ylabel="", add_legend=False,
                                   cache_df_fn="main_results_data/front_runners_kat+wo_2018_wo.pickle",
                                   output_table_filename="front_runners_kat+wo_2018_wo.html")


    plt.ylim((ymin, ymax))

    try:
        plt.tight_layout(pad=0.1)
        plt.savefig("front_runners_kat+wo_2018.pdf")
    except Exception as e:
        print("RuntimeError while trying to layout and/or save plot, probably pdflatex could not be found or it couldn't find a font.")
        if reraise:
            raise e


# # 2018: Drees, Colijn & Romme
drees = (lt_df['complete achternaam'] == 'Drees')
colijn = (lt_df['complete achternaam'] == 'Colijn')

romme = (ltpw_df['complete achternaam'] == 'Romme')

drees_qsq = piq.phrases_str_from_persons_simple(lt_df[drees])
colijn_qsq = piq.phrases_str_from_persons_simple(lt_df[colijn])
romme_qsq = piq.phrases_str_from_persons_simple(ltpw_df[romme])

with mpl.style.context(['./fullwidth.mplstyle']):
    df, fig, ax = pib.plot_kldiv_search_per_zuil(drees_qsq, wo_min, period_max, doc_count_mn[wo_min:],
                               es, index_kb, zuilen_filters=zuilen_filters, style=pistyle.style_dict4_noProLib,
                               title="", output=True,
                               figsize=None, xlabel="", ylabel="", add_legend=False,
                               agg=piq.arts_per_mn_agg,
                               cache_df_fn="main_results_data/drees.pickle",
                               output_table_filename="drees.html")
    ax.axvline('1946-5-16', **pistyle.axline_style)
    ax.axvline('1948-7-7', **pistyle.axline_style)
    ax.axvline('1952-6-25', **pistyle.axline_style)
    ax.axvline('1956-6-13', **pistyle.axline_style)
    ax.axvline('1959-3-12', **pistyle.axline_style)
    ax.axvline('1963-5-15', **pistyle.axline_style)

    plt.xticks(['1948-7-7', '1952-6-25', '1956-6-13', '1959-3-12', '1963-5-15'], ['7 juli 1948', '25 juni 1952', '13 juni 1956', '12 maart 1959', '15 mei 1963'])
 
    dx = 10/72.
    dy = -1.3/72. 
    offset = mpl.transforms.ScaledTranslation(dx, dy, fig.dpi_scale_trans)

    # apply offset transform to all x ticklabels.
    label = ax.xaxis.get_majorticklabels()[3]
    label.set_transform(label.get_transform() + offset)

    try:
        plt.tight_layout(pad=0.1)
        plt.savefig("Drees.pdf")
    except Exception as e:
        print("RuntimeError while trying to layout and/or save plot, probably pdflatex could not be found or it couldn't find a font.")
        if reraise:
            raise e

    df, fig, ax = pib.plot_kldiv_search_per_zuil(colijn_qsq, period_min, ib_max, doc_count_mn[:ib_max],
                                   es, index_kb, zuilen_filters=zuilen_filters, style=pistyle.style_dict5,
                                   title="", output=True,
                                   figsize=None, xlabel="", ylabel="", add_legend=False,
                                   agg=piq.arts_per_mn_agg,
                                   cache_df_fn="main_results_data/colijn.pickle",
                               output_table_filename="colijn.html")
    ax.axvline('1922-7-5', **pistyle.axline_style)
    ax.axvline('1925-7-1', **pistyle.axline_style)
    ax.axvline('1929-7-3', **pistyle.axline_style)
    ax.axvline('1933-4-26', **pistyle.axline_style)
    ax.axvline('1937-5-26', **pistyle.axline_style)

    plt.xticks(['1922-7-5', '1925-7-1', '1929-7-3', '1933-4-26', '1937-5-26'],
               ['5 juli 1922', '1 juli 1925', '3 juli 1929', '26 april 1933', '26 mei 1937'])

    try:
        plt.tight_layout(pad=0.1)
        plt.savefig("Colijn.pdf")
    except Exception as e:
        print("RuntimeError while trying to layout and/or save plot, probably pdflatex could not be found or it couldn't find a font.")
        if reraise:
            raise e

    df, fig, ax = pib.plot_kldiv_search_per_zuil(romme_qsq, wo_min, period_max, doc_count_mn[wo_min:],
                                   es, index_kb, zuilen_filters=zuilen_filters, style=pistyle.style_dict4_noProLib,
                                   title="", output=True,
                                   figsize=None, xlabel="", ylabel="", add_legend=False,
                                   agg=piq.arts_per_mn_agg,
                                   cache_df_fn="main_results_data/romme.pickle",
                               output_table_filename="romme.html")

    ax.axvline('1946-5-16', **pistyle.axline_style)
    ax.axvline('1948-7-7', **pistyle.axline_style)
    ax.axvline('1952-6-25', **pistyle.axline_style)
    ax.axvline('1956-6-13', **pistyle.axline_style)
    ax.axvline('1959-3-12', **pistyle.axline_style)
    ax.axvline('1963-5-15', **pistyle.axline_style)

    plt.xticks(['1948-7-7', '1952-6-25', '1956-6-13', '1959-3-12', '1963-5-15'], ['7 juli 1948', '25 juni 1952', '13 juni 1956', '12 maart 1959', '15 mei 1963'])

    dx = 10/72.
    dy = -1.3/72. 
    offset = mpl.transforms.ScaledTranslation(dx, dy, fig.dpi_scale_trans)

    # apply offset transform to all x ticklabels.
    label = ax.xaxis.get_majorticklabels()[3]
    label.set_transform(label.get_transform() + offset)

    try:
        plt.tight_layout(pad=0.1)
        plt.savefig("Romme.pdf")
    except Exception as e:
        print("RuntimeError while trying to layout and/or save plot, probably pdflatex could not be found or it couldn't find a font.")
        if reraise:
            raise e


# # Organizations (non-political)

org.df['begin'] = pd.to_datetime(org.df['begin'], errors='coerce')
org.df['eind'] = pd.to_datetime(org.df['eind'], errors='coerce')
org.df['naam_begin'] = pd.to_datetime(org.df['naam_begin'], errors='coerce')
org.df['naam_eind'] = pd.to_datetime(org.df['naam_eind'], errors='coerce')

period_min_dt = dateutil.parser.parse(period_min)
period_max_dt = dateutil.parser.parse(period_max)

period_org = np.where(org.df["begin"] != pd.NaT,      org.df["begin"] < period_max_dt,      True) *              np.where(org.df["eind"] != pd.NaT,       org.df["eind"] > period_min_dt,       True) *              np.where(org.df["naam_begin"] != pd.NaT, org.df["naam_begin"] < period_max_dt, True) *              np.where(org.df["naam_eind"] != pd.NaT,  org.df["naam_eind"] > period_min_dt,  True)

org_period = org.df[period_org]

org_min_media = list(org_period.type.unique())
org_min_media.remove('media')


# with mpl.style.context(['./square.mplstyle']):
#     pib.plot_search_alle_zuilen_select_org_type(org_period, list(org_period.type.unique()),
#                                                 period_min, period_max, doc_count,
#                                                 None, es, index_kb, tile_size=None,
#                                                 zuilen_filters=zuilen_filters, style=style, lang="nl",
#                                                 suptitle="", labels=labels, xlabel='jaar',
#                                    cache_df_fn_base="main_results_data/org_all.pickle")
#     plt.tight_layout(pad=0.1)
#     plt.savefig("org_all.pdf")

#     pib.plot_search_alle_zuilen_select_org_type(org_period, 'media', period_min, period_max, doc_count,
#                                                 None, es, index_kb, layout=(2,2), tile_size=None,
#                                                 zuilen_filters=zuilen_filters, style=style, lang="nl",
#                                                 suptitle="", labels=labels, xlabel='jaar',
#                                    cache_df_fn_base="main_results_data/org_media.pickle")
#     plt.tight_layout(pad=0.1)
#     plt.savefig("org_media.pdf")

#     pib.plot_search_alle_zuilen_select_org_type(org_period, 'cultureel', period_min, period_max, doc_count,
#                                             None, es, index_kb, layout=(2,2), tile_size=None,
#                                             zuilen_filters=zuilen_filters, style=style, lang="nl",
#                                             suptitle="", labels=labels, xlabel='jaar',
#                                             cache_df_fn_base="main_results_data/org_cultureel.pickle")
#     plt.tight_layout(pad=0.1)
#     plt.savefig("org_cultureel.pdf")

#     pib.plot_search_alle_zuilen_select_org_type(org_period, 'vak/soc.econ', period_min, period_max, doc_count,
#                                             None, es, index_kb, layout=(2,2), tile_size=None,
#                                             zuilen_filters=zuilen_filters, style=style, lang="nl",
#                                             suptitle="", labels=labels, xlabel='jaar',
#                                             cache_df_fn_base="main_results_data/org_vakbond_soc_econ.pickle")
#     plt.tight_layout(pad=0.1)
#     plt.savefig("org_vakbond_soc_econ.pdf")

#     pib.plot_search_alle_zuilen_select_org_type(org_period, org_min_media, period_min, period_max, doc_count,
#                                                 None, es, index_kb, layout=(2,2), tile_size=None,
#                                                 zuilen_filters=zuilen_filters, style=style, labels=labels, xlabel='jaar',
#                                                 suptitle="", lang="nl",
#                                    cache_df_fn_base="main_results_data/org_all_except_media.pickle",
#                                    add_legends=[0,], legends_args={0:{'ncol':2}})
#     plt.tight_layout(pad=0.1)
#     plt.savefig("org_all_except_media.pdf")


with mpl.style.context(['./subfig49.mplstyle']):
    fig, ax = plt.subplots(1, 1)

    pib.plot_search_alle_zuilen_select_org_type(org_period, org_min_media, period_min, ib_max, doc_count,
                                                None, es, index_kb, layout=(1,1), tile_size=None,
                                                zuilen_filters=zuilen_filters, style=style, labels=labels, xlabel='jaar',
                                                suptitle="", lang="nl",
                                   apply_period=True, ax=ax,
                                   cache_df_fn_base="main_results_data/org_all_except_media_kat.pickle",
                                   add_legends=[], legends_args={},
                                   plot_zuilen=['kat'], subtitles=False)

    pib.plot_search_alle_zuilen_select_org_type(org_period, org_min_media, wo_min, period_max, doc_count,
                                                None, es, index_kb, layout=(1,1), tile_size=None,
                                                zuilen_filters=zuilen_filters, style=pistyle.style_dict4_noProLib, labels=labels, xlabel='jaar',
                                                suptitle="", lang="nl",
                                   apply_period=True, ax=ax,
                                   cache_df_fn_base="main_results_data/org_all_except_media_kat.pickle",
                                   add_legends=[], legends_args={},
                                   plot_zuilen=['kat'], subtitles=False)

    ymin, ymax = plt.ylim()

    try:
        plt.tight_layout(pad=0.1)
        plt.savefig("org_all_except_media_kat.pdf")
    except Exception as e:
        print("RuntimeError while trying to layout and/or save plot, probably pdflatex could not be found or it couldn't find a font.")
        if reraise:
            raise e

    fig, ax = plt.subplots(1, 1)

    pib.plot_search_alle_zuilen_select_org_type(org_period, org_min_media, period_min, ib_max, doc_count,
                                                None, es, index_kb, layout=(1,1), tile_size=None,
                                                zuilen_filters=zuilen_filters, style=style, labels=labels, xlabel='jaar',
                                                suptitle="", lang="nl",
                                   apply_period=True, ax=ax,
                                   cache_df_fn_base="main_results_data/org_all_except_media_soc.pickle",
                                   add_legends=[], legends_args={},
                                   plot_zuilen=['soc'], subtitles=False)

    pib.plot_search_alle_zuilen_select_org_type(org_period, org_min_media, wo_min, period_max, doc_count,
                                                None, es, index_kb, layout=(1,1), tile_size=None,
                                                zuilen_filters=zuilen_filters, style=pistyle.style_dict4_noProLib, labels=labels, xlabel='jaar',
                                                suptitle="", lang="nl",
                                   apply_period=True, ax=ax,
                                   cache_df_fn_base="main_results_data/org_all_except_media_soc.pickle",
                                   add_legends=[], legends_args={},
                                   plot_zuilen=['soc'], subtitles=False)

    plt.ylim((ymin, ymax))

    try:
        plt.tight_layout(pad=0.1)
        plt.savefig("org_all_except_media_soc.pdf")
    except Exception as e:
        print("RuntimeError while trying to layout and/or save plot, probably pdflatex could not be found or it couldn't find a font.")
        if reraise:
            raise e


# # Ideological terms

# with mpl.style.context(['./subfig49.mplstyle']):
#     pib.plot_kldiv_search_per_zuil(pidilib.data.ideologische_termen.qual['interbellum']["SDAP"],
#                                    period_min, ib_max, doc_count_ib,
#                                    es, index_kb, zuilen_filters=zuilen_filters, style=style,
#                                    # title="Socialist ideological terms - Interbellum period",
#                                    title="", add_legend=False,
#                                    figsize=None, xlabel="jaar", ylabel="",
#                                    cache_df_fn="main_results_data/ideo_soc_ib.pickle")
#     plt.tight_layout(pad=0.1)
#     plt.savefig("ideo_soc_ib.pdf")

#     pib.plot_kldiv_search_per_zuil(pidilib.data.ideologische_termen.qual['wederopbouw']["PvdA"],
#                                    wo_min, period_max, doc_count_wo,
#                                    es, index_kb, zuilen_filters=zuilen_filters, style=style,
#                                    # title="Socialist ideological terms - Wederopbouw period",
#                                    title="", figsize=None, add_legend=False, xlabel="jaar", ylabel="",
#                                    cache_df_fn="main_results_data/ideo_soc_wo.pickle")
#     plt.tight_layout(pad=0.1)
#     plt.savefig("ideo_soc_wo.pdf")

#     pib.plot_kldiv_search_per_zuil(pidilib.data.ideologische_termen.qual['interbellum']["RKSP"],
#                                    period_min, ib_max, doc_count_ib,
#                                    es, index_kb, zuilen_filters=zuilen_filters, style=style,
#                                    # title="Catholic ideological terms - Interbellum period",
#                                    title="", figsize=None, add_legend=False, xlabel="jaar", ylabel="",
#                                    cache_df_fn="main_results_data/ideo_kat_ib.pickle")
#     plt.tight_layout(pad=0.1)
#     plt.savefig("ideo_kat_ib.pdf")

#     pib.plot_kldiv_search_per_zuil(pidilib.data.ideologische_termen.qual['wederopbouw']["KVP"],
#                                    wo_min, period_max, doc_count_wo,
#                                    es, index_kb, zuilen_filters=zuilen_filters, style=style,
#                                    # title="Catholic ideological terms - Wederopbouw period",
#                                    title="", figsize=None, add_legend=False, xlabel="jaar", ylabel="",
#                                    cache_df_fn="main_results_data/ideo_kat_wo.pickle")
#     plt.tight_layout(pad=0.1)
#     plt.savefig("ideo_kat_wo.pdf")

#     pib.plot_kldiv_search_per_zuil(pidilib.data.ideologische_termen.qual['wederopbouw']["KVP_optimal"],
#                                    wo_min, period_max, doc_count_wo,
#                                    es, index_kb, zuilen_filters=zuilen_filters, style=style,
#                                    # title="Catholic ideological terms - Optimized - Wederopbouw",
#                                    title="", figsize=None, xlabel="jaar", ylabel="", add_legend=False,
#                                    cache_df_fn="main_results_data/ideo_kat_wo_opt.pickle")
#     plt.tight_layout(pad=0.1)
#     plt.savefig("ideo_kat_wo_opt.pdf")

#     pib.plot_kldiv_search_per_zuil(pidilib.data.ideologische_termen.qual['interbellum']["lib"],
#                                    period_min, ib_max, doc_count_ib,
#                                    es, index_kb, zuilen_filters=zuilen_filters, style=style,
#                                    # title="Liberal ideological terms - Interbellum period",
#                                    title="", figsize=None, add_legend=False, xlabel="jaar", ylabel="",
#                                    cache_df_fn="main_results_data/ideo_lib_ib.pickle")
#     plt.tight_layout(pad=0.1)
#     plt.savefig("ideo_lib_ib.pdf")

#     pib.plot_kldiv_search_per_zuil(pidilib.data.ideologische_termen.qual['interbellum']["lib_optimal"],
#                                    period_min, ib_max, doc_count_ib,
#                                    es, index_kb, zuilen_filters=zuilen_filters, style=style,
#                                    # title="Liberal ideological terms - Optimized - Interbellum",
#                                    title="", xlabel="jaar", ylabel="",
#                                    figsize=None, add_legend=False,
#                                    cache_df_fn="main_results_data/ideo_lib_ib_opt.pickle")
#     plt.tight_layout(pad=0.1)
#     plt.savefig("ideo_lib_ib_opt.pdf")

# arp_terms = "\n".join([pidilib.data.ideologische_termen.qual['interbellum']["ARP"],
#                        pidilib.data.ideologische_termen.qual['wederopbouw']["ARP"]])

# with mpl.style.context(['./subfig49.mplstyle']):
#     pib.plot_kldiv_search_per_zuil(arp_terms,
#                                    period_min, period_max, doc_count,
#                                    es, index_kb, zuilen_filters=zuilen_filters, style=pistyle.style_dict4,
#                                    # title="Protestant (ARP) ideological terms",
#                                        title="", xlabel="jaar", ylabel="", add_legend=False,
#                                        figsize=None,
#                                    cache_df_fn="main_results_data/ideo_ARP.pickle")
#     plt.tight_layout(pad=0.1)
#     plt.savefig("ideo_ARP.pdf")

#     pib.plot_kldiv_search_per_zuil(pidilib.data.ideologische_termen.qual['interbellum']["CHU"],
#                                    period_min, period_max, doc_count,
#                                    es, index_kb, zuilen_filters=zuilen_filters, style=pistyle.style_dict4,
#                                    # title="Protestant (CHU) ideological terms",
#                                        title="", xlabel="jaar", ylabel="", add_legend=False,
#                                        figsize=None,
#                                    cache_df_fn="main_results_data/ideo_CHU.pickle")
#     plt.tight_layout(pad=0.1)
#     plt.savefig("ideo_CHU.pdf")


# Two periods combined into one panel

with mpl.style.context(['./subfig49.mplstyle']):
    fig, ax = plt.subplots(1, 1)

    pib.plot_kldiv_search_per_zuil(pidilib.data.ideologische_termen.qual['interbellum']["SDAP"],
                                   period_min, ib_max, doc_count_ib,
                                   es, index_kb, zuilen_filters=zuilen_filters, style=style,
                                   # title="Socialist ideological terms - Interbellum period",
                                   title="", add_legend=False,
                                   figsize=None, xlabel="jaar", ylabel="", ax=ax,
                                   cache_df_fn="main_results_data/ideo_soc_ib.pickle",
                               output_table_filename="ideo_soc_ib.html")

    pib.plot_kldiv_search_per_zuil(pidilib.data.ideologische_termen.qual['wederopbouw']["PvdA"],
                                   wo_min, period_max, doc_count_wo,
                                   es, index_kb, zuilen_filters=zuilen_filters, style=pistyle.style_dict4_noProLib,
                                   # title="Socialist ideological terms - Wederopbouw period",
                                   title="", figsize=None, add_legend=False, xlabel="jaar", ylabel="",
                                   ax=ax,
                                   cache_df_fn="main_results_data/ideo_soc_wo.pickle",
                               output_table_filename="ideo_soc_wo.html")

    ymin, ymax = plt.ylim()
    
    try:
        plt.tight_layout(pad=0.1)
        plt.savefig("ideo_soc.pdf")
    except Exception as e:
        print("RuntimeError while trying to layout and/or save plot, probably pdflatex could not be found or it couldn't find a font.")
        if reraise:
            raise e


    fig, ax = plt.subplots(1, 1)

    pib.plot_kldiv_search_per_zuil(pidilib.data.ideologische_termen.qual['interbellum']["RKSP"],
                                   period_min, ib_max, doc_count_ib,
                                   es, index_kb, zuilen_filters=zuilen_filters, style=style,
                                   # title="Catholic ideological terms - Interbellum period",
                                   title="", figsize=None, add_legend=False, xlabel="jaar", ylabel="",
                                   ax=ax,
                                   cache_df_fn="main_results_data/ideo_kat_ib.pickle",
                               output_table_filename="ideo_kat_ib.html")

    pib.plot_kldiv_search_per_zuil(pidilib.data.ideologische_termen.qual['wederopbouw']["KVP_optimal"],
                                   wo_min, period_max, doc_count_wo,
                                   es, index_kb, zuilen_filters=zuilen_filters, style=pistyle.style_dict4_noProLib,
                                   # title="Catholic ideological terms - Optimized - Wederopbouw",
                                   title="", figsize=None, xlabel="jaar", ylabel="", add_legend=False,
                                   ax=ax,
                                   cache_df_fn="main_results_data/ideo_kat_wo_opt.pickle",
                               output_table_filename="ideo_kat_wo_opt.html")

    plt.ylim((ymin, ymax))
    
    try:
        plt.tight_layout(pad=0.1)
        plt.savefig("ideo_kat.pdf")
    except Exception as e:
        print("RuntimeError while trying to layout and/or save plot, probably pdflatex could not be found or it couldn't find a font.")
        if reraise:
            raise e



