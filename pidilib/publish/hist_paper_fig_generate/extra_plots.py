#!/usr/bin/env python

# coding: utf-8

# Extra plots:
# - three panel one for the BMGN paper

import sys
sys.path.append('../../..')

import matplotlib as mpl
mpl.use('pgf')
from matplotlib import pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
from elasticsearch import Elasticsearch
import certifi
import dateutil

import pidilib.elastic.queries as piq
import pidilib.bamboo as pib
import pidilib.data.partijen
import pidilib.publish.style as pistyle
# import pidilib.publish.pretty as pipretty
# prep = pipretty.pretty
import pidilib.data.personen
import pidilib.data.organisaties as org
import pidilib.data.ideologische_termen

import os


def center_year_bins_labels(ax):
    bins = np.int32([int(s.text()) for s in ax.get_xticklabels()])
    ax.set_xticks(bins + 0.5)
    ax.set_xticklabels(bins)


# CONFIGURATION
period_min = "1918-07-03"  # elections parliament
ib_max = "1940-05-10"  # invasion
# period_min = "1946-07-03"  # start cabinet Beel I
wo_min = "1946-05-16"  # elections parliament
period_max = "1967-02-15"  # elections parliament

SERVER = ['elastic']
PORT = 9200
INDEX_NAME = 'kb_pidimehs'

# sns.set_context('talk', font_scale=1.2)
# sns.set_style('darkgrid')

zuilen_filters = piq.zuilen_kranten_filters_v5
style = pistyle.style_dict4_noPro

es = Elasticsearch(
    SERVER,
    port=PORT,
    timeout=600
)

index_kb = INDEX_NAME

eng_label = {'kat': 'Catholic', 'soc': 'Socialist', 'lib': 'Liberal', 'pro': 'Protestant',
             'neu': 'neutral', 'total': 'total', 'totaal': 'total'}
NL_label = {'kat': 'Katholiek', 'soc': 'Socialistisch', 'lib': 'Liberaal', 'pro': 'Protestants',
             'neu': 'neutraal', 'total': 'totaal', 'totaal': 'totaal'}

cols = list(style['raw'][0][:-1])
labels = [NL_label[col] for col in cols]


def generate_normalization_data():
    # All documents per year per pillar
    filter_zuil = lambda zuil: piq.and_filter([zuilen_filters[zuil],
                                               piq.date_range_filter(period_min, period_max)])
    opdracht_zuil = lambda zuil: {"size": 0,
                                  "query": piq.filtered_query(piq.all_query, filter_zuil(zuil)),
                                  "aggs": piq.arts_per_yr_agg}

    doc_count_zuil = []

    for zuil_naam in zuilen_filters.keys():
        result_date_hist = es.search(index=index_kb, body=opdracht_zuil(zuil_naam))
        df = pib.df_from_arts_per_t_agg(result_date_hist, opdracht_zuil(zuil_naam), period_min, period_max,
                                        col_name=zuil_naam)
        doc_count_zuil.append(df)

    doc_count = pd.concat(doc_count_zuil, axis=1)
    doc_count["totaal"] = doc_count.sum(axis=1)

    # ax = pib.ppzuil(doc_count, style['raw'], size_str='small')

    # Interbellum
    filter_zuil = lambda zuil: piq.and_filter([zuilen_filters[zuil],
                                               piq.date_range_filter(period_min, ib_max)])
    opdracht_zuil = lambda zuil: {"size": 0,
                                  "query": piq.filtered_query(piq.all_query, filter_zuil(zuil)),
                                  "aggs": piq.arts_per_yr_agg}

    doc_count_zuil_ib = []

    for zuil_naam in zuilen_filters.keys():
        result_date_hist = es.search(index=index_kb, body=opdracht_zuil(zuil_naam))
        df = pib.df_from_arts_per_t_agg(result_date_hist, opdracht_zuil(zuil_naam), period_min, ib_max,
                                        col_name=zuil_naam)
        doc_count_zuil_ib.append(df)

    doc_count_ib = pd.concat(doc_count_zuil_ib, axis=1)
    doc_count_ib["totaal"] = doc_count_ib.sum(axis=1)

    # ax = pib.ppzuil(doc_count_ib, style['raw'], size_str='small')

    # Wederopbouw
    filter_zuil = lambda zuil: piq.and_filter([zuilen_filters[zuil],
                                               piq.date_range_filter(wo_min, period_max)])
    opdracht_zuil = lambda zuil: {"size": 0,
                                  "query": piq.filtered_query(piq.all_query, filter_zuil(zuil)),
                                  "aggs": piq.arts_per_yr_agg}

    doc_count_zuil_wo = []

    for zuil_naam in zuilen_filters.keys():
        result_date_hist = es.search(index=index_kb, body=opdracht_zuil(zuil_naam))
        df = pib.df_from_arts_per_t_agg(result_date_hist, opdracht_zuil(zuil_naam), wo_min, period_max,
                                        col_name=zuil_naam)
        doc_count_zuil_wo.append(df)

    doc_count_wo = pd.concat(doc_count_zuil_wo, axis=1)
    doc_count_wo["totaal"] = doc_count_wo.sum(axis=1)

    # ax = pib.ppzuil(doc_count_wo, style['raw'], size_str='small')


    # MONTHLY NORMALIZATION

    # All documents per month per pillar
    filter_zuil = lambda zuil: piq.and_filter([zuilen_filters[zuil],
                                               piq.date_range_filter(period_min, period_max)])
    opdracht_zuil_mn = lambda zuil: {"size": 0,
                                    "query": piq.filtered_query(piq.all_query, filter_zuil(zuil)),
                                    "aggs": piq.arts_per_mn_agg}

    doc_count_zuil_mn = []

    for zuil_naam in zuilen_filters.keys():
        result_date_hist = es.search(index=index_kb, body=opdracht_zuil_mn(zuil_naam))
        df = pib.df_from_arts_per_t_agg(result_date_hist, opdracht_zuil_mn(zuil_naam), period_min, period_max,
                                        col_name=zuil_naam)
        doc_count_zuil_mn.append(df)

    doc_count_mn = pd.concat(doc_count_zuil_mn, axis=1)
    doc_count_mn["totaal"] = doc_count_mn.sum(axis=1)

    # pib.ppzuil(doc_count_mn, style['raw'], size_str='medium')

    # plt.show()

    try:
        os.mkdir('main_results_data')
    except OSError:
        print("data directory already exists")

    doc_count.to_pickle('main_results_data/df-articles_total.pickle')
    doc_count_ib.to_pickle('main_results_data/df-articles_total_ib.pickle')
    doc_count_wo.to_pickle('main_results_data/df-articles_total_wo.pickle')
    doc_count_mn.to_pickle('main_results_data/df-articles_total_month.pickle')


def load_normalization_data():
    doc_count = pd.read_pickle('main_results_data/df-articles_total.pickle')
    doc_count_ib = pd.read_pickle('main_results_data/df-articles_total_ib.pickle')
    doc_count_wo = pd.read_pickle('main_results_data/df-articles_total_wo.pickle')
    doc_count_mn = pd.read_pickle('main_results_data/df-articles_total_month.pickle')

    return doc_count, doc_count_ib, doc_count_wo, doc_count_mn


# generate_normalization_data()
doc_count, doc_count_ib, doc_count_wo, doc_count_mn = load_normalization_data()


mpl.style.use(['seaborn-paper', 'seaborn-white', 'seaborn-deep',
               './pidimehs_paper_hi16.mplstyle', './line_plot.mplstyle'])

plt.rcParams.update({
    'pgf.rcfonts': False,
    'pgf.preamble': [
        r"\usepackage{fontspec}",
        r"\setmainfont{Lexicon No1 Roman A Med}",
    ],
    'font.family': 'Lexicon No1',
    # "mathtext.default": "regular",
    # "mathtext.it": "regular",
    # "mathtext.rm": "regular",
    # "mathtext.tt": "regular",
    # "mathtext.bf": "regular",
    # "mathtext.cal": "regular",
    # "mathtext.sf": "regular",
})



# # Party names

soc_parties = pidilib.data.partijen.sdap_v5 + "\n" + pidilib.data.partijen.pvda_v5

df, fig, ax = pib.plot_search_per_zuil(soc_parties, period_min, ib_max,
                                       doc_count, [], es, index_kb, output=True,
                                       figsize=(6.299212813062128, 2.25),
                                       zuilen_filters=zuilen_filters,
                                       style=style, xlabel="jaar",
                                       add_legend=False, lang="nl",
                                       cache_df_fn="main_results_data/party_names_soc.pickle")
ax[0].set_ylim(0, ax[0].get_ylim()[1])
ax[1].set_ylim(0, ax[1].get_ylim()[1])
# for axi in ax:
    # center_year_bins_labels(axi)

plt.tight_layout(pad=0.1)
plt.savefig("party_names_soc_3panel.pdf")
