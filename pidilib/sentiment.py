from __future__ import print_function

import errno
import io
import json
import os
import os.path

import certifi
from elasticsearch import Elasticsearch
from elasticsearch.helpers import scan
from toolz import take

from data import partijen
from elastic.queries import (and_filter, date_range_filter, filtered_query,
                             query_string_query, zuilen_filters)
qsq = query_string_query


MAX_HITS = 1000


ES_SEARCH_CONFIG = {'hosts': ['pidimehs.lucy.surfsara.nl'],
                    'http_auth': (USERNAME, PASSWORD),
                    'port': 443, 'use_ssl': True,
                    'verify_certs': True, 'ca_certs': certifi.where(),
                    'timeout': 600}
es = Elasticsearch(**ES_SEARCH_CONFIG)


def mk(*parties):
    terms = "\n".join(parties)
    return qsq(" OR ".join('"%s"' % x for x in terms.splitlines()))


zuil_query = {
    "soc": mk(partijen.sdap, partijen.pvda),
    "kat": mk(partijen.rksp, partijen.abrkv, partijen.kvp),
    "pro": mk(partijen.chu, partijen.arp),
    #"lib": mk(partijen.lu, partijen.bvl, partijen.eb, partijen.vblsp,
    #          partijen.vdb)
    "lib": mk(partijen.liberalen),
}


del zuilen_filters['neutraal']  # Geen lijstje partijen beschikbaar.


def md(path):
    try:
        dir = os.path.join("annot", path)
        os.mkdir(dir)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise
    finally:
        return dir


artikel_filter = {"term": {"article_dc_subject": "artikel"}}


md('')
for zuil, filter in zuilen_filters.items():
    md(zuil)

    for andere in zuilen_filters:
        if andere == zuil:
            continue

        dir = md(os.path.join(zuil, andere))
        query = filtered_query(zuil_query[andere],
                               and_filter([filter,
                                           artikel_filter,
                                           #date_range_filter("1900", "1940")]))
                                           date_range_filter("1946", "1975")]))
        query = {"query": query}

        print(zuil, "->", andere,
              es.search(body=query, index='kb', size=0)['hits']['total'])

        for hit in take(MAX_HITS, scan(es, query)):
            hit = hit['_source']

            path = os.path.join(dir, hit['identifier'] + '.txt')
            with io.open(path, 'w') as out:
                out.write(hit['text_content'])
